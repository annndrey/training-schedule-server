#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import CheckConstraint, ForeignKey
from sqlalchemy.orm import backref, validates, relationship
from sqlalchemy.ext.hybrid import hybrid_property
from passlib.apps import custom_app_context as pwd_context
from itsdangerous import URLSafeSerializer, BadSignature, SignatureExpired
from flask import current_app, jsonify
from flask_login import UserMixin
import datetime
import enum
import jwt
import calendar
import math

db = SQLAlchemy()

def lastdate():
    d = datetime.date.today()
    _, lastday = calendar.monthrange(d.year, d.month)
    return datetime.date(d.year, d.month, lastday)

dayweek = {1: "пн",
           2: "вт",
           3: "ср",
           4: "чт",
           5: "пт",
           6: "сб",
           7: "вс"
}

revdayweek = {v: k for k, v in dayweek.items()}

class Gender(enum.Enum):
    m = u'm'
    f = u'f'
    n = 'na'


class LessonType(enum.Enum):
    group = u'группа'
    personal = u'индивидуально'
    split = u'сплит (2 человека)'
    mini = u'мини-группа (до 4 человек)'

    
class VisitType(enum.Enum):
    multi = u'абонемент'
    single = u'разово'

    
class StaffType(enum.Enum):
    junior = u'младший'
    middle = u'средний'
    senior = u'старший'
    admin = u'администратор'

    
class PriceType(enum.Enum):
    promo = u'Промо'
    free = u'Бесплатно'
    single = 'Разовое'
    ticket = u'Абонемент'
    personal = u'Персональное'
    debt = u"Долг"
    

student_groups = db.Table('student_groups', db.Model.metadata,
                          db.Column('student_id', db.Integer, db.ForeignKey('student.id')),
                          db.Column('student_group_id', db.Integer, db.ForeignKey('student_group.id')),
                          db.Column('daysowweek', db.String(255)),
                          db.PrimaryKeyConstraint('student_id', 'student_group_id'))


student_lessons = db.Table('student_lessons', db.Model.metadata,
                           db.Column('student_id', db.Integer, db.ForeignKey('student.id')),
                           db.Column('lesson_id', db.Integer, db.ForeignKey('lesson.id')),
                           db.PrimaryKeyConstraint('student_id', 'lesson_id'))

#class Registration(db.Model):
#    # here we store lessons that users are registered for
#    # user id
#    # lesson id
#    # to get gym, day time etc
#    # many students can be registered to one lesson and
#    id = db.Column(db.Integer, primary_key=True)
#    lesson_id = db.Column(db.Integer, ForeignKey('lesson.id'))
#    lesson = relationship("Lesson", backref=backref("registration"))
#    student_id = db.Column(db.Integer, ForeignKey('student.id'))
#    student = relationship("Student", backref=backref("registration"))


class Price(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text())
    pricetype = db.Column(db.Enum(PriceType), nullable=False, default=PriceType.ticket)
    price = db.Column(db.Float)
    note = db.Column(db.Text())
    numvisits = db.Column(db.Integer, default=0)
    validdays = db.Column(db.Integer)

    
class Ticket(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # tickettype
    #price = db.Column(db.Float)
    price_id = db.Column(db.Integer, ForeignKey('price.id'))
    price = relationship("Price", backref=backref("tickets"))
    note = db.Column(db.Text())
    date = db.Column(db.Date(), default=datetime.datetime.now().date())
    # at the end of release month
    ends = db.Column(db.Date(), default=lastdate)
    visitsleft = db.Column(db.Integer, default=0)
    student_id = db.Column(db.Integer, ForeignKey('student.id'))
    student = relationship("Student", backref=backref("tickets", order_by='Ticket.id.desc()'))
    cancels = db.Column(db.Integer, default=0)
    transfers= db.Column(db.Integer, default=1)
    is_paid = db.Column(db.Boolean, default=False)
    # we cancel a ticket when we add a new one
    is_cancelled = db.Column(db.Boolean, default=False)
    payment_id = db.Column(db.Integer, ForeignKey('payment.id'))
    
    @hybrid_property
    def isvalid(self):
        # we don't account visitsleft for now!
        today = datetime.date.today()
        if  self.date <= today <= self.ends and not self.is_cancelled and self.is_paid:
            return True
        return False


class Staff(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    login = db.Column(db.String(400))
    name = db.Column(db.String(400))
    password_hash = db.Column(db.String(400))
    note = db.Column(db.Text(), nullable=True)
    is_confirmed = db.Column(db.Boolean, default=False)
    confirmed_on = db.Column(db.DateTime, default=False)
    registered_on = db.Column(db.DateTime, default=datetime.datetime.today)
    superadmin = db.Column(db.Boolean, default=False)
    admin = db.Column(db.Boolean, default=False)
    trainer = db.Column(db.Boolean, default=False)
    phone = db.Column(db.String(400))
    stafftype = db.Column(db.Enum(StaffType), nullable=True, default=StaffType.junior)
    company_id = db.Column(db.Integer, ForeignKey('company.id', ondelete='SET NULL'), nullable=True)
    # price rates
    grouprate = db.Column(db.Integer)
    personrate = db.Column(db.Integer)
    personalrate = db.Column(db.Integer)
    splitrate = db.Column(db.Integer)
    minirate = db.Column(db.Integer)
    archived = db.Column(db.Boolean, default=False)
    chatid = db.Column(db.String(400))
    subscribed = db.Column(db.Boolean, default=False)
    
    @hybrid_property
    def coeff(self):
        # salary coeff
        if self.stafftype == StaffType.junior:
            return 0.5
        elif self.stafftype == StaffType.middle:
            return 1.0
        elif self.stafftype == StaffType.senior:
            return 1.5
        
    @validates('login')
    def validate_login(self, key, login):
        if len(login) > 1:
            assert '@' in login, 'Invalid email'
        return login
    
    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        if not self.archived:
            return pwd_context.verify(password, self.password_hash)

    def generate_auth_token(self):
        token = jwt.encode({
        'sub': self.login,
        'iat': datetime.datetime.utcnow(),
        'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=60)},
        current_app.config['SECRET_KEY'])
        return token

    

    @staticmethod
    def verify_auth_token(token):
        s = URLSafeSerializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None 
        except BadSignature:
            return None 
        user = Staff.query.get(data['id'])
        if not user.archived:
            return user


class Gym(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(400))
    address = db.Column(db.String(400))
    phone = db.Column(db.String(100))
    company_id = db.Column(db.Integer, ForeignKey('company.id'))
    url = db.Column(db.Text())
    color = db.Column(db.String(100))
    note = db.Column(db.Text())
    
    
class Company(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(400))
    address = db.Column(db.String(400))
    phone = db.Column(db.String(100))
    # trainers - company - many to one
    staff = relationship('Staff', backref='company')
    # gyms - company - many to one
    gyms = relationship('Gym', backref='company')

    
class Student(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(400))
    address = db.Column(db.String(400))
    email = db.Column(db.String(400))
    phone = db.Column(db.String(100))
    gender = db.Column(db.Enum(Gender))
    bdate = db.Column(db.Date)
    group_id = db.Column(db.Integer, ForeignKey('student_group.id'))
    note = db.Column(db.Text())
    contract_id = db.Column(db.Integer)
    is_confirmed = db.Column(db.Boolean, default=False)
    is_deleted = db.Column(db.Boolean, default=False)
    confirmed_on = db.Column(db.DateTime, default=False)
    registered_on = db.Column(db.DateTime, default=datetime.datetime.today)
    staff_id = db.Column(db.Integer, ForeignKey('staff.id'))
    added_by = relationship(Staff)
    parent_name = db.Column(db.String(400))
    parent_phone = db.Column(db.String(400))
    parent_email = db.Column(db.String(400))
    grade = db.Column(db.String(400), default='')
    parent_bdate = db.Column(db.Date)

    @hybrid_property
    def trainers(self):
        return "/".join(set([l.trainer.name for l in self.lessons ]))
    
    @hybrid_property
    def lastvisit(self):
        if len(self.visit) > 0:
            return self.visit[-1].datetime
        
    @hybrid_property
    def rarevisitor(self):
        rarevisitor = True
        if self.visit and len(self.visit) > 0:
            delta = datetime.datetime.now() - self.visit[-1].datetime
            if delta.days <= 10:
                rarevisitor = False

        return rarevisitor

    
    @hybrid_property
    def beginner(self):
        beginner = True
        if self.visit and len(self.visit) > 1:
            beginner = False

        return beginner

    @hybrid_property
    def lastvalidticket(self):
        if len(self.tickets) > 0:
            validtickets = [t for t in self.tickets if t.isvalid]
            if len(validtickets) > 0:
                return validtickets[-1]
            else:
                return []
        return False

    @hybrid_property
    def validtickets(self):
        if len(self.tickets) > 0:
            validtickets = [t for t in self.tickets if t.isvalid]
            return validtickets
        return []

    @hybrid_property
    def get_debt(self):
        debt = 0
        debttickets = [t.visitsleft for t in self.tickets if t.price.pricetype == PriceType.debt and t.visitsleft < 1]

        if len(debttickets) > 0:
            debt = sum(debttickets)
            
        return debt
    
    
class Lesson(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    note = db.Column(db.Text())
    start = db.Column(db.DateTime())
    end = db.Column(db.DateTime())
    #training_day_id = db.Column(db.Integer, ForeignKey('training_day.id'))
    gym_id = db.Column(db.Integer, ForeignKey('gym.id'))
    gym = relationship("Gym", backref='lessons')
    group_id = db.Column(db.Integer, ForeignKey('student_group.id'))
    group = relationship("StudentGroup", backref='lessons')
    trainer_id = db.Column(db.Integer, ForeignKey('staff.id'))
    trainer = relationship(Staff, foreign_keys=[trainer_id])
    strainer_id = db.Column(db.Integer, ForeignKey('staff.id'))
    strainer = relationship(Staff, foreign_keys=[strainer_id])
    students = relationship(Student, backref='lessons',  secondary=student_lessons)
    lessontype = db.Column(db.Enum(LessonType), nullable=False, default=LessonType.group)
    # добавляем сюда students по умолчанию из group.students
    # и разово при переносах и добавлении новых студентов
    # тогда не надо хранить дни и залы в билетах,
    # и можно делать один билет.
    # как посмотреть куда записан человек?
    # student.studentgroups
    # как посмотреть, сколько занятий у него осталось?
    # ticket.visitsleflt
    # как посмотреть, по каким дням ему ходить?
    # for gr in student.studentgroups:
    #     lesson.start lesson.end for lesson in gr.lessons if student in lesson.students
    # как записать на занятие?
    # просто добавляем ученика в Lesson students
    # 
    # Решили проблему с переносами, отменами, и пр
    # мб. сделать журнал переносов, куда записывать
    # с какого на какое перенесено занятие? и если человек отменил, туда
    # запишем что он отменил? но это уже отдельно

    
class Payment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date())
    student_id = db.Column(db.Integer, ForeignKey('student.id'))
    student = relationship("Student", backref=backref("payments", order_by='Payment.id.desc()'))
    payer = db.Column(db.Text)
    purpose = db.Column(db.Text)
    bank = db.Column(db.Text)
    amount = db.Column(db.Integer)
    quantity = db.Column(db.Float)
    price_id = db.Column(db.Integer, ForeignKey('price.id'))
    price = relationship("Price", backref=backref("payments", order_by='Payment.id.desc()'))
    confirmed = db.Column(db.Boolean, default=False)
    saved = db.Column(db.Boolean, default=False)
    guessed = db.Column(db.Boolean, default=False)
    staff_id = db.Column(db.Integer, ForeignKey('staff.id'))
    edited_by = relationship("Staff", backref=backref("payments", order_by='Payment.id.desc()'))
    edited = db.Column(db.DateTime(), default=datetime.datetime.now())
    ticket = relationship("Ticket", backref='payment')
    
    
class StudentGroup(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(400))
    # trainer
    trainer_id = db.Column(db.Integer, ForeignKey('staff.id'))
    trainer = relationship(Staff, foreign_keys=[trainer_id])
    students = relationship(Student, backref='studentgroup',  secondary=student_groups)
    minage = db.Column(db.Integer)
    maxage = db.Column(db.Integer)
    strainer_id = db.Column(db.Integer, ForeignKey('staff.id'))
    strainer = relationship(Staff, foreign_keys=[strainer_id])
    gym_id = db.Column(db.Integer, ForeignKey('gym.id'))
    gym = relationship(Gym, foreign_keys=[gym_id])
    
    @hybrid_property
    def weekdays(self):
        wdays = [dayweek[d] for d in set(sorted([l.start.isoweekday() for l in self.lessons if l.start.month == datetime.datetime.today().month]))]
        nstudents = dict.fromkeys(wdays, [])

        for d in wdays:
            mlessons = [len(l.students) for l in self.lessons if l.start.month == datetime.datetime.today().month and l.start.isoweekday() == revdayweek[d]]
            nstudents[d] = round(sum(mlessons)/len(mlessons))
        res = nstudents
        return res

    @hybrid_property
    def numstudents(self):
        return len(self.students)

    @hybrid_property
    def dow(self):
        return self.daysowweek

    
class LessonVisit(db.Model):
    # Lesson visit log
    id = db.Column(db.Integer, primary_key=True)
    lesson_id = db.Column(db.Integer, ForeignKey('lesson.id', ondelete='CASCADE'))
    # TODO: Keep orphaned visits? 
    lesson = relationship("Lesson", backref=backref("visit", uselist=False, cascade="all, delete-orphan"))
    ticket_id = db.Column(db.Integer, ForeignKey('ticket.id'))
    # TODO: Keep orphaned visits? 
    ticket = relationship("Ticket", backref=backref("visit"))
    
    visited = db.Column(db.Boolean, default=True)
    student_id = db.Column(db.Integer, ForeignKey('student.id'))
    student = relationship("Student", backref=backref("visit", uselist=True))
    cancelled = db.Column(db.Boolean, default=False)
    note = db.Column(db.Text)
    lessontype = db.Column(db.Enum(LessonType), nullable=False, default=LessonType.group)
    datetime = db.Column(db.DateTime())
    visittype = db.Column(db.Enum(VisitType), nullable=False, default=VisitType.multi)
    staff_id = db.Column(db.Integer, ForeignKey('staff.id'))
    edited_by = relationship("Staff", backref=backref("visit", uselist=False))
    edited = db.Column(db.DateTime())
