DEBUG = True
DEVELOPMENT = True
SECRET_KEY='mysupersecretkeyy'
SQLALCHEMY_DATABASE_URI='mysql+pymysql://dbuser:dbpassword@dbhost/dbname'
SQLALCHEMY_TRACK_MODIFICATIONS = False
