#!/usr/bin/env python
# -*- coding: utf-8 -*-

from functools import wraps
from flask import Flask, g, make_response, request, current_app, send_file
from flask_restful import Resource, Api, reqparse, abort, marshal_with
from flask.json import jsonify
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import or_
from sqlalchemy import func as sql_func
from flask_marshmallow import Marshmallow
from flask_httpauth import HTTPBasicAuth
from flask_cors import CORS, cross_origin
from flask_restful.utils import cors
from marshmallow import fields
from marshmallow_enum import EnumField
from models import db, Staff, Gym, Company, Student, StudentGroup, Lesson, LessonVisit, Ticket, PriceType, Gender, StaffType, Price, LessonType, VisitType, Payment
import click
import datetime
import calendar
from dateutil.relativedelta import relativedelta
import jwt
import json
import pandas as pd
import io
import time
from io import BytesIO

from celery import Celery

# schedule parsing:
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import requests
from bs4 import BeautifulSoup

import re


app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origins": "*"}}, support_credentials=True, methods=['GET', 'POST', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'])
api = Api(app, prefix="/api/v1")
auth = HTTPBasicAuth()
app.config.from_envvar('APPSETTINGS')


app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'
app.config['CELERY_RESULT_BACKEND'] = 'redis://localhost:6379/0'
#celery = Celery(app.name, broker=app.config['CELERY_BROKER_URL'])
#celery.conf.update(app.config)



app.config['PROPAGATE_EXCEPTIONS'] = True
BOT_KEY = app.config['BOT_KEY']
CHAT_ID = app.config['CHAT_ID']
db.init_app(app)
migrate = Migrate(app, db)
ma = Marshmallow(app)
pd.set_option('display.max_colwidth', -1)

transl_enum = {"group": "группа",
               "personal": "индивидуально",
               "split": "сплит",
               "mini": "мини-группа"
}

dayweek = {1: "пн",
           2: "вт",
           3: "ср",
           4: "чт",
           5: "пт",
           6: "сб",
           7: "вс"
}

pd.options.display.float_format = '{:,.0f}'.format


def make_celery(app):
    celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'])
    celery.conf.update(app.config)
    TaskBase = celery.Task
    class ContextTask(TaskBase):
        abstract = True
        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask
    return celery


celery = make_celery(app)

def addstudentstonewlessons():
    # add students for the current month's lessons
    groups = db.session.query(StudentGroup).all()
    for g in groups:
        for lesson in g.lessons:
            if lesson.start >= datetime.datetime(2019, 2, 1): 
                for student in g.students:
                    if student not in lesson.students:
                        print(lesson.start, student.name, lesson.group.name, lesson.trainer.name, lesson.gym.name, student.studentgroup, lesson.group)
                        lesson.students.append(student)
                        db.session.add(lesson)
                        db.session.commit()

@celery.task
def importlessonsfromurl(url):
    print("IMPORT DATA")
    r = requests.get(url, timeout=20)
    c = r.content
    gym_id = url.split("=")[-1]
    soup = BeautifulSoup(c, features="html.parser")
    gym = soup.find("div", class_="title-club").find("form").find("option", value=gym_id).getText()

    gym_db = db.session.query(Gym).filter(Gym.name == gym).first()

    print("IMPORT", url, gym, gym_db.name)
    columns = soup.find_all("div", class_="column")
    for cl in columns:
        cells = cl.find_all("div", class_="cell")
        daystart = dayend = None
        sitelessons = []
        for cell in cells:
            if cell.has_attr("data-event-start"):
                datestart = datetime.datetime.strptime(cell["data-event-start"], '%d.%m.%Y %H:%M:%S')
                dateend = datetime.datetime.strptime(cell["data-event-end"], '%d.%m.%Y %H:%M:%S')
                lnk = cell.findAll('a')[-1]
                data = {"id":lnk['data-id']}
                resp = requests.post('https://o-skal.ru/ajax/raspisanie-ajax.php', data=data)
                sp = BeautifulSoup(resp.text, features="html.parser")
                headers = sp.findAll('p', class_="h2")
                card_trainer = sp.findAll('p', class_="nobr")
                c_trainer = card_trainer[-1].text.strip().split()[0].strip()
                trainer = c_trainer
                if len(headers) > 0:
                    headers = headers[0]
                hours = re.findall("\d.:\d.", headers.text)
                #print("HEADERS", headers.text)
                #print("HOURS", hours)
                if len(hours) > 1:
                    hours = list(map(int, hours[-1].split(":")))
                    # Fixing wrong lesson end
                    
                    dateend = dateend.replace(hour=hours[0], minute=hours[1])
                    daystart = datestart.replace(hour=1, minute=0)
                    dayend = dateend.replace(hour=23, minute=59)

                #trainer = cell.find("p", class_="s-trainers").text.strip().split()[0].strip()
                trainer_db = [s for s in db.session.query(Staff).all() if s.name.split()[-1] == c_trainer]
                #if "Медведев" in card_trainer:
                #print("Trainer found", card_trainer, trainer_db)
                if len(trainer_db) > 0:
                    group = cell["data-event-title"]
                    group = group[group.find("(")+1:group.find(")")].capitalize()
                    prevlesson = db.session.query(Lesson).filter(Lesson.trainer == trainer_db[-1]).filter(Lesson.gym == gym_db).filter(Lesson.start == datestart).filter(Lesson.end == dateend).first()
                    if prevlesson:
                        grouptrainer = prevlesson.trainer
                    else:
                        grouptrainer = trainer_db[-1]
                    #print("Looking for group", group)    
                    group_db = db.session.query(StudentGroup).filter(StudentGroup.name == group).filter(StudentGroup.gym == gym_db).filter(StudentGroup.trainer == grouptrainer).first()
                    
                    if not group_db:
                        #print("NO GROUP", group, trainer, datestart, dateend)
                        # create new group
                        group_db = StudentGroup(name=group, trainer=grouptrainer, gym=gym_db)
                        db.session.add(group_db)
                        db.session.commit()
                        #print("NEW GROUP CREATED", group, trainer, )
                        
                    

                    if prevlesson and prevlesson.group is None:
                        #print("ADDING GROUP TO EXISTING LESSON", group_db)
                        prevlesson.group = group_db
                        db.session.add(prevlesson)
                        db.session.commit()
                        
                    #print("CELL", datestart, dateend, trainer, trainer_db, group, group_db, gym)
            
                    if not prevlesson:
                        newlesson = Lesson(start=datestart,
                                           end=dateend,
                                           gym=gym_db,
                                           lessontype='group',
                                           trainer=trainer_db[-1],
                                           group=group_db
                        )
                        if group_db.strainer:
                            newlesson.strainer = group_db.strainer
                      
                        db.session.add(newlesson)
                        db.session.commit()
                        #print(newlesson)
                        for st in group_db.students:
                            if st not in newlesson.students:
                                newlesson.students.append(st)
                        if newlesson not in sitelessons:
                            sitelessons.append(newlesson)
                    else:
                        if prevlesson not in sitelessons:
                            sitelessons.append(prevlesson)
        if daystart:
            todaylessons = db.session.query(Lesson).filter(Lesson.start >= daystart).filter(Lesson.end <= dayend).filter(Lesson.gym == gym_db).all()
            todelete = list(set(todaylessons) - set(sitelessons))
            # только групповые занятия!!
            # индивидуалки и мини не отображаются на сайте
            for l in todelete:
                if l.lessontype == 'group':
                    #print("To delete", l.gym.name, l.trainer.name, l.start, l.end, l.group)
                    #print("NOT ACTUALLY DELETING")
                    db.session.delete(l)
                    db.session.commit()
                #else:
                    #print("Not deleting", l.lessontype.name, l.gym.name, l.trainer.name, l.start, l.end, l.group)
    print("Import DONE for {}".format(url))

def calcsalary(row, **kwargs):
    role = kwargs.get('role', 'trainer')

    sal = 0
    if role == 'trainer':
        if row.lessontype == "группа":
            if pd.isnull(row.grouprate):
                sal = row.visit * row.personrate
            else:
                sal = row.grouprate + row.visit * row.personrate
            
        elif row.lessontype == "индивидуально":
            sal = row.visit * row.personalrate
        elif row.lessontype == "мини":
            sal = row.visit * row.minirate
        elif row.lessontype == "сплит":
            sal = row.visit * row.splitrate
    else:
        sal = row.st_grouprate

    return sal


def lastdate():
    d = datetime.date.today()
    _, lastday = calendar.monthrange(d.year, d.month)
    return datetime.date(d.year, d.month, lastday)


def token_required(f):  
    @wraps(f)
    def _verify(*args, **kwargs):
        auth_headers = request.headers.get('Authorization', '').split()

        invalid_msg = {
            'message': 'Invalid token. Registeration and / or authentication required',
            'authenticated': False
        }
        expired_msg = {
            'message': 'Expired token. Reauthentication required.',
            'authenticated': False
        }

        if len(auth_headers) != 2:
            return abort(403)

        #try:
        token = auth_headers[1]
        try:
            data = jwt.decode(token, current_app.config['SECRET_KEY'], options={'verify_exp': False})
        except:
            abort(403)
        user = Staff.query.filter_by(login=data['sub']).first()

        if user.archived or not user:
            abort(404)
        else:
            return f(*args, **kwargs)
        #@except jwt.ExpiredSignatureError:
        #   return abort(401) # 401 is Unauthorized HTTP status code
        #except (jwt.InvalidTokenError, Exception) as e:
        #    return abort(405)

    return _verify


def authenticate(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if not getattr(func, 'authenticated', True):
            return func(*args, **kwargs)

        acct = basic_authentication()  # custom account lookup function

        if acct:
            return func(*args, **kwargs)

        abort(401)
    return wrapper


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)

@auth.error_handler
def unauthorized():
    return make_response(jsonify({'error': 'Unauthorized access'}), 401)

@auth.verify_password
def verify_password(username_or_token, password):
    user = Staff.verify_auth_token(username_or_token)
    if not user:
        # try to authenticate with username/password
        user = db.session.query(Staff).filter(Staff.login == username_or_token).filter(Staff.archived != True).first()
        if not user or not user.verify_password(password) or user.archived:
            return False
    g.user = user
    return True


@app.route('/api/v1/token', methods=['POST'])
@cross_origin(supports_credentials=True)
def get_auth_token_post():
    username = request.json.get('username')
    password = request.json.get('password')
    user = Staff.query.filter_by(login = username).first()
    if user:
        if user.verify_password(password):
            token = user.generate_auth_token()
            response = jsonify({ 'token': "%s" % token.decode('utf-8'), "user_id":user.id, "login": user.login, "name": user.name, "admin": user.admin, "trainer": user.trainer, "superadmin": user.superadmin })
            return response
    abort(404)


@app.route('/api/v1/token', methods=['GET'])
#@auth.login_required
def get_auth_token():
    token = g.user.generate_auth_token()
    return jsonify({ 'token': "%s" % token })


# SCHEMAS 
class StaffSchema(ma.ModelSchema):
    class Meta:
        model = Staff
    stafftype = EnumField(StaffType)
    coeff = ma.Function(lambda obj: obj.coeff)

    
class GymSchema(ma.ModelSchema):
    class Meta:
        model = Gym


class CompanySchema(ma.ModelSchema):
    class Meta:
        model = Company

    staff = ma.Nested(StaffSchema, many=True)
    gyms = ma.Nested(GymSchema, many=True)


class PriceSchema(ma.ModelSchema):
    pricetype = EnumField(PriceType)

    class Meta:
        model = Price

    
# /ticket * * * * * * * * * * * * * * * * * * *
class TicketSchema(ma.ModelSchema):
    price = ma.Nested('PriceSchema', exclude=("tickets",))    
    class Meta:
        model = Ticket

    isvalid = ma.Function(lambda obj: obj.isvalid)

# /visit * * * * * * * * * * * * * * * * * * *
class LessonVisitSchema(ma.ModelSchema):
    class Meta:
        model = LessonVisit
        
    lesson = ma.Nested('LessonSchema', exclude=("students", "lesson"))
    lessontype = EnumField(LessonType)
    visittype = EnumField(VisitType)
    student = ma.Nested('LessonStudentSchema', exclude=("lessons", "visit", "tickets"))
    edited_by = ma.Nested('StaffSchema', only=("id", "name"))
    ticket = ma.Nested('TicketSchema', only=('id', 'price'))
    
class LessonStudentSchema(ma.ModelSchema):
    gender = EnumField(Gender)
    class Meta:
        model = Student

    lastvalidticket = ma.Nested('TicketSchema')
    
# /student * * * * * * * * * * * * * * * * * * *


class PaymentSchema(ma.ModelSchema):
    class Meta:
        model = Payment

    student = ma.Nested('StudentSchema', only=("id", 'name', 'contract_id'))
    price = ma.Nested('PriceSchema', only=("id", 'name'))
    edited_by = ma.Nested('StaffSchema', only=("id", 'name'))

    
class StudentSchema(ma.ModelSchema):
    gender = EnumField(Gender)
    
    class Meta:
        model = Student

    trainer = ma.Method('get_trainer_name')
    age = ma.Method('calc_age')
    lastvalidticket = ma.Function(lambda obj: obj.lastvalidticket)
    rarevisitor = ma.Function(lambda obj: obj.rarevisitor)
    beginner = ma.Function(lambda obj: obj.beginner)
    trainers = ma.Function(lambda obj: obj.trainers)
    lastvisit = ma.Function(lambda obj: obj.lastvisit)
    get_debt = ma.Function(lambda obj: obj.get_debt)
    visit = ma.Nested(LessonVisitSchema, many=True)
    studentgroup = ma.Nested('StudentGroupSchema', many=True, exclude=('students', 'lessons', 'strainer'))
    tickets = ma.Nested(TicketSchema, many=True)
    lessons = ma.Nested('LessonSchema', many=True, exclude=('students',))
    validtickets = ma.Nested(TicketSchema, many=True, exclude=('price',))
    group_ids = ma.Function(lambda obj: [g.id for g in obj.studentgroup])
    lastvalidticket = ma.Nested('TicketSchema')
    added_by = ma.Nested('StaffSchema', only=("id", 'name',))

    def calc_age(self, obj):
        if obj.bdate:
            today = datetime.date.today()
            if obj.bdate != '0000-00-00':
                delta = relativedelta(today, obj.bdate).years
                return delta
        
    def get_trainer_name(self, obj):
        return obj.added_by.name

    
# /lesson * * * * * * * * * * * * * * * * * * *
class LessonSchema(ma.ModelSchema):
    class Meta:
        model = Lesson

    students = ma.Nested(LessonStudentSchema, many=True, exclude=("lessons", "tickets", "visit", "added_by", "address", "bdate", "confirmed_on", "note", "phone", "registered_on"))
    lessontype = EnumField(LessonType)
    trainer = ma.Function(lambda obj: obj.trainer.name)
    trainer_id = ma.Function(lambda obj: obj.trainer.id)
    strainer = ma.Function(lambda obj: obj.strainer.name)
    strainer_id = ma.Function(lambda obj: obj.strainer.id)
    gym = ma.Function(lambda obj: obj.gym.name)
    # recalculate depending on ticket
    numstudents = ma.Method("calcstudents")
    gym_id = ma.Function(lambda obj: obj.gym.id)
    date = ma.Function(lambda obj: obj.day.date)
    date_id = ma.Function(lambda obj: obj.day.id)
    group = ma.Function(lambda obj: obj.group.name)
    group_id = ma.Function(lambda obj: obj.group.id)
    group_minage = ma.Function(lambda obj: obj.group.minage)
    group_maxage = ma.Function(lambda obj: obj.group.maxage)
    title = ma.Function(lambda obj: obj.trainer.name + ("\nгруппа: " +obj.group.name if obj.group else "\nиндивидуально") + "\n" + obj.gym.name +  ("\n" + obj.note if obj.note else "") )
    newEvent = ma.Function(lambda obj: "false")
    dow = ma.Function(lambda obj: obj.day.date.weekday())
    #students = ma.Method("filter_students")
    color = ma.Function(lambda obj: obj.gym.color)

    def calcstudents(self, obj):
        return len(obj.students)

class LessonsSchema(ma.ModelSchema):
    class Meta:
        model = Lesson

    #students = ma.Nested(LessonStudentSchema, many=True, exclude=("lessons", "tickets", "visit", "added_by", "address", "bdate", "confirmed_on", "note", "phone", "registered_on"))
    lessontype = EnumField(LessonType)
    trainer = ma.Function(lambda obj: obj.trainer.name)
    trainer_id = ma.Function(lambda obj: obj.trainer.id)
    strainer = ma.Function(lambda obj: obj.strainer.name)
    strainer_id = ma.Function(lambda obj: obj.strainer.id)
    gym = ma.Function(lambda obj: obj.gym.name)
    # recalculate depending on ticket
    numstudents = ma.Method("calcstudents")
    gym_id = ma.Function(lambda obj: obj.gym.id)
    date = ma.Function(lambda obj: obj.day.date)
    date_id = ma.Function(lambda obj: obj.day.id)
    group = ma.Function(lambda obj: obj.group.name)
    group_id = ma.Function(lambda obj: obj.group.id)
    group_minage = ma.Function(lambda obj: obj.group.minage)
    group_maxage = ma.Function(lambda obj: obj.group.maxage)
    title = ma.Function(lambda obj: obj.trainer.name + ("\nгруппа: " +obj.group.name if obj.group else "\nиндивидуально") + "\n" + obj.gym.name +  ("\n" + obj.note if obj.note else "") )
    newEvent = ma.Function(lambda obj: "false")
    dow = ma.Function(lambda obj: obj.day.date.weekday())
    #students = ma.Method("filter_students")
    color = ma.Function(lambda obj: obj.gym.color)

    def calcstudents(self, obj):
        return len(obj.students)


    

# /stgroup * * * * * * * * * * * * * * * * * * *
class StudentGroupsSchema(ma.ModelSchema):
    class Meta:
        model = StudentGroup
    gym = ma.Nested('GymSchema', exclude = ("lessons", "students"))
    numstudents = ma.Function(lambda obj: obj.numstudents)
    weekdays = ma.Function(lambda obj: obj.weekdays)
    trainer = ma.Nested(StaffSchema, only=("id", 'name',))
    #weekdays = ma.Function(lambda obj: " ".join([dayweek[d] for d in set(sorted([l.start.isoweekday() for l in obj.lessons if l.start.month == datetime.datetime.today().month]))]))
    
class StudentGroupSchema(ma.ModelSchema):
    class Meta:
        model = StudentGroup

    # name = ma.Function(lambda obj: "{}, {} чел".format(obj.name, len(obj.students)))
    # students = ma.Nested(StudentSchema, many=True, exclude=("studentgroup", ""))
    trainer = ma.Nested('StaffSchema', exclude=("password_hash",))
    weekdays = ma.Function(lambda obj: obj.weekdays)
    dow = ma.Function(lambda obj: g.dow)
    # TODO change exclude to only?
    strainer = ma.Nested('StaffSchema', exclude=("password_hash",))
    gym = ma.Nested('GymSchema', exclude = ("lessons", "students"))
    # lessons = ma.Nested(LessonSchema, many=True)
    # numstudents = #ma.Function(lambda obj: obj.numstudents)
    trainer_id = ma.Function(lambda obj: obj.trainer.id)
    strainer_id = ma.Function(lambda obj: obj.strainer.id)
    days = ma.Function(lambda obj: [l.day.date.weekday() for l in obj.lessons])

    
# SINGLE API

class ImportsAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()

    def options(self, *args, **kwargs):
        return jsonify([])


    @token_required
    @cross_origin()
    def post(self):
        auth_headers = request.headers.get('Authorization', '').split()
        token = auth_headers[1]
        data = jwt.decode(token, current_app.config['SECRET_KEY'], options={'verify_exp': False})
        user = Staff.query.filter_by(login=data['sub']).first()

        if user.admin:
            urlstring = "https://o-skal.ru/raspisanie/?club={}"
            # Gym IDs
            gyms = [3, 2337, 2338, 431, 3601, 3638, 3639]
            #gyms = [3, ]
            urls = [urlstring.format(n) for n in gyms]

            for u in urls:
                r = importlessonsfromurl.delay(u)
            addstudentstonewlessons()
            return jsonify('Done'), 201
        
        abort(400, 'No data provided')

class StatsDetailsAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
    
    def options(self, *args, **kwargs):
        return jsonify([])

    @token_required
    @cross_origin()
    def get(self):
        auth_headers = request.headers.get('Authorization', '').split()
        token = auth_headers[1]
        data = jwt.decode(token, current_app.config['SECRET_KEY'], options={'verify_exp': False})
        user = Staff.query.filter_by(login=data['sub']).first()

        if not user:
            abort(404)
        obj = request.args['obj']
        year, month = map(int, request.args['date'].split('-'))
        fday, lday = calendar.monthrange(year, month)
        fday = 1
        firstdate = datetime.datetime(year, month, fday, 0, 1)
        lastdate = datetime.datetime(year, month, lday, 23, 59)
        cl = calendar.Calendar(0)
        
        monthdays = dict([(c[0], dayweek[c[1]+1]) for c in cl.itermonthdays2(year, month) if c[0]!=0])
        monthvisits = db.session.query(LessonVisit)\
                                .join(Lesson)\
                                .filter(Lesson.start >= firstdate)\
                                .filter(Lesson.start <= lastdate)\
                                .filter(LessonVisit.visited == True)
        
        if user.trainer:
            monthvisits = monthvisits.filter(LessonVisit.lesson.has(Lesson.trainer==user))
            
        monthvisits = monthvisits.all()
        datadict = { 'visit':[],
                     'lesson':[],
                     'lessondate':[],
                     'lessonday':[],
                     'trainer': [],
                     'strainer': [],
                     'trainerstatus':[],
                     'student': [],
                     'lessontype': [],
                     'gym': [],
                     'visittype': [],
                     'contractnum': []
        }
        
        for v in monthvisits:
            trname = v.lesson.trainer.name
            strnr = None
            if v.lesson.strainer:
                strnr = v.lesson.strainer.name
            datadict['strainer'].append(strnr)
            datadict['visit'].append(v.id)
            datadict['lesson'].append(v.lesson.start.strftime("%Y-%m-%d %H:%M"))
            datadict['lessondate'].append(v.lesson.start.strftime("%Y-%m-%d"))
            datadict['lessonday'].append(monthdays[int(v.lesson.start.strftime("%-d"))])
            trname = v.lesson.trainer.name.split()[-1]
            trinitials = "".join([i[0] + "." for i in v.lesson.trainer.name.split()[:-1]])
            trname = trname + " " + trinitials
            datadict['trainer'].append(trname)
            datadict['trainerstatus'].append(v.lesson.trainer.stafftype.value)
            datadict['contractnum'].append(v.student.contract_id)
            datadict['student'].append(v.student.name)
            datadict['lessontype'].append(v.lesson.lessontype.value)
            datadict['gym'].append(v.lesson.gym.name)
            datadict['visittype'].append(v.visittype.value)

        if monthvisits:
            
            df = pd.DataFrame.from_dict(datadict)
            print(obj)
            if obj == 'gyms':
                # детализация по площадкам
                gymvisits = df.groupby(['gym', 'lessondate', 'lessonday', 'visittype']).visit.count().to_frame().reset_index()
                #gymsum = df.groupby(['gym', 'lessondate', 'lessonday']).visit.count().to_frame().reset_index()
                #gymsum.columns = ['gym',  'lessondate', 'lessonday',  'totalday']
                #gymvisits = gymvisits.merge(gymsum, how='outer')
                #print(gymvisits)
                # gym  lessondate lessonday  visittype  visit  totalday
                outtable = pd.pivot_table(gymvisits, values='visit', index=['gym', 'visittype'], columns=['lessondate', 'lessonday'], aggfunc = "sum", margins=True).fillna('')

            elif obj == 'trainers':
                gymvisits = df.groupby(['trainer', 'lessondate', 'lessonday', 'lessontype', 'visittype']).visit.count().to_frame().reset_index()
                #gymsum = df.groupby(['gym', 'lessondate', 'lessonday']).visit.count().to_frame().reset_index()
                #gymsum.columns = ['gym',  'lessondate', 'lessonday',  'totalday']
                #gymvisits = gymvisits.merge(gymsum, how='outer')
                #print(gymvisits)
                # gym  lessondate lessonday  visittype  visit  totalday
                outtable = pd.pivot_table(gymvisits, values='visit', index=['trainer', 'lessontype', 'visittype'], columns=['lessondate', 'lessonday'], aggfunc = "sum", margins=True).fillna('')
                
                # gymdatevisits.columns.set_levels(['b1','c1','f1'],level=1,inplace=True)
            #gyemdatevisits = gymdatevisits.unstack(['lessondate', 'lessonday']).fillna(value=0)
            
            # gymdatevisits.loc['Total',:]= gymdatevisits.sum(axis=0)
            # sum by columns
            #sumdays = gymdatevisits.sum(level=[0, 1]).sum(axis=0)
            # sum by row
            #sumgyms = gymdatevisits.sum(level=[0, 1]).sum(axis=1)
            
            #sumdays = pd.DataFrame(sumdays['visit'].to_frame(name='sumbyday').reset_index()).T
            #sdlist = list(sumdays.iloc[2])
            #sdlist = pd.DataFrame(sdlist, index=gymdatevisits['visit'].columns).T
            #print(sdlist)
            ##gymdatevisits['visit'].iloc[1] = sdlist
            #print(gymdatevisits)#['visit'].append(sdlist))


            
            output = BytesIO()
            writer = pd.ExcelWriter(output, engine='xlsxwriter')
            outtable.to_excel(writer, startrow = 0, merge_cells = True, sheet_name = "Sheet_1")
            workbook = writer.book
            worksheet = writer.sheets["Sheet_1"]
            writer.close()
            output.seek(0)
            resp = make_response(send_file(output, attachment_filename="file.xlsx", as_attachment=True))
            resp.headers['Content-Disposition'] = 'attachment'
            resp.headers['filename'] = 'gym_date_visits.xlsx'
            # >>> TODO: add sum by visittype 
            return resp
        
            # print(df)
        # return ""

class StatsAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
    
    def options(self, *args, **kwargs):
        return jsonify([])

    @token_required
    @cross_origin()
    def get(self, id=None):
        """
        Тренер получает за выход + за каждого ребёнка, это при групповых занятиях,
        пт- персональная тренировка, здесь фиксированная сумма, 
        сплит по 600 за каждого
        second trainer - только за выход, для групповых занятий
        """
        
        auth_headers = request.headers.get('Authorization', '').split()
        token = auth_headers[1]
        data = jwt.decode(token, current_app.config['SECRET_KEY'], options={'verify_exp': False})
        user = Staff.query.filter_by(login=data['sub']).first()

        if not user:
            abort(404)

        year, month = map(int, request.args['date'].split('-'))
        # return all stats for every trainer
        fday, lday = calendar.monthrange(year, month)

        #if fday == 0:
        fday = 1
        firstdate = datetime.datetime(year, month, fday, 0, 1)
        lastdate = datetime.datetime(year, month, lday, 23, 59)

        monthvisits = db.session.query(LessonVisit)\
                                .join(Lesson)\
                                .filter(Lesson.start >= firstdate)\
                                .filter(Lesson.start <= lastdate)\
                                .filter(LessonVisit.visited == True)
                            

        if user.admin:
            monthvisits = monthvisits.all()
        elif user.trainer:
            monthvisits = monthvisits.filter(LessonVisit.lesson.has(Lesson.trainer==user))
            monthvisits = monthvisits.all()
                
        datadict = { 'visit':[],
                     'lesson':[],
                     'trainer': [],
                     'strainer': [],
                     'trainerstatus':[],
                     'student': [],
                     'lessontype': [],
                     'tickettype': [],
                     'gym': [],
                     'grouprate': [],
                     'personrate': [],
                     'personalrate': [],
                     'splitrate': [],
                     'minirate': [],
                     'visittype': [],
                     'st_grouprate': [],
                     'contractnum': []
        }
        
        
        for v in monthvisits:
            # прямо тут можно считать зарплату.
            # добавлять в какой-нибудь trainerdic,
            # key = trainername
            trname = v.lesson.trainer.name
            strnr = None
            if v.lesson.strainer:
                strnr = v.lesson.strainer.name
            datadict['strainer'].append(strnr)
            
            datadict['visit'].append(v.id)
            datadict['lesson'].append(v.lesson.start.strftime("%Y-%m-%d %H:%M"))
            trname = v.lesson.trainer.name.split()[-1]
            trinitials = "".join([i[0] + "." for i in v.lesson.trainer.name.split()[:-1]])
            trname = trname + " " + trinitials
            datadict['trainer'].append(trname)
            datadict['trainerstatus'].append(v.lesson.trainer.stafftype.value)
            datadict['contractnum'].append(v.student.contract_id)
            datadict['student'].append(v.student.name)
            datadict['lessontype'].append(v.lesson.lessontype.value)
            datadict['tickettype'].append(v.ticket.price.pricetype.value)
            datadict['gym'].append(v.lesson.gym.name)
            # rates:
            datadict['grouprate'].append(v.lesson.trainer.grouprate)
            datadict['personrate'].append(v.lesson.trainer.personrate)
            datadict['personalrate'].append(v.lesson.trainer.personalrate)
            datadict['splitrate'].append(v.lesson.trainer.splitrate)
            datadict['minirate'].append(v.lesson.trainer.minirate)
            datadict['visittype'].append(v.visittype.value)
            # strainer rates
            if v.lesson.strainer:
                datadict['st_grouprate'].append(v.lesson.strainer.grouprate)
            else:
                datadict['st_grouprate'].append(None)
                
        if monthvisits:    
            df = pd.DataFrame.from_dict(datadict)
            trainerrates = df[['trainer', 'grouprate', 'personrate', 'personalrate', 'splitrate', 'minirate']]
            strainerrates = df[['strainer', 'st_grouprate']].drop_duplicates()
            # кол-во посетивших учеников
            totalvisits = int(df.visit.count())
            totallessons = df.lesson.nunique()
            totalvisitsbytickettype = df.groupby('tickettype').visit.count()
            totallessonsbylessntype = df.groupby('lessontype').lesson.nunique()
            studentvisits = df.groupby('trainer').student.count()
            studentlessons = df.groupby(['contractnum', 'student', 'lessontype', 'lesson']).visit.count()
            gymdatevisits = df.groupby(['gym', 'lessontype', 'lesson']).visit.count()
            trainerdatevisits = df.groupby(['trainer', 'lessontype', 'lesson']).visit.count()
            lessontypesvisits = df.groupby(['trainer', 'lessontype', 'lesson']).visit.count()
            trainerlessons = df.groupby(['trainer', 'lessontype']).lesson.nunique()
            trainergyms = df.groupby(['trainer', 'gym']).visit.count()
            gymvisits = df.groupby(('gym', 'visittype')).visit.count()
            trainerrates = trainerrates.drop_duplicates()
            lessons = df.groupby(['trainer', 'lessontype', 'lesson']).visit.count().reset_index()
            strainer_lessons = df.groupby(['strainer', 'lessontype', 'lesson']).visit.count().reset_index()
            salary = pd.merge(lessons, trainerrates, on='trainer')
            salary['salary'] = salary.apply(calcsalary, axis=1, role='trainer')

            salary = salary.groupby('trainer').salary.sum()
            strainer_salary = pd.merge(strainer_lessons, strainerrates)
            
            if not strainer_salary.empty:
                strainer_salary['salary'] = strainer_salary.apply(calcsalary, axis=1, role='strainer')
                strainer_salary = strainer_salary.groupby('strainer').salary.sum()

            # Final results
            res = {}
            studentlessons = studentlessons.to_frame()
            studentlessons = studentlessons.drop(labels='visit', axis=1)
            res['Детализация по посещениям учеников'] = studentlessons.to_html(header=False, border=0, classes=['table', 'table-striped', 'table-hover', 'table-sm'], bold_rows=False)
            res['Детализация посещений по площадкам'] = gymdatevisits.to_frame().to_html(header=False, border=0, classes=['table', 'table-striped', 'table-hover', 'table-sm'], bold_rows=False)
            res['зал/посещения/тип'] = gymvisits.to_frame().to_html(header=False, border=0, classes=['table', 'table-striped', 'table-hover', 'table-sm'], bold_rows=False)
            res['зарплата'] = salary.to_frame().to_html(header=False, border=0, classes=['table', 'table-striped', 'table-hover', 'table-sm'], bold_rows=False)
            
            if user.admin:
                if not strainer_salary.empty:
                    res['зарплата стажерам'] = strainer_salary.to_frame().to_html(header=False, border=0, classes=['table', 'table-striped', 'table-hover', 'table-sm'], bold_rows=False)
                res['посещения'] =  studentvisits.to_frame().to_html(header=False, border=0, classes=['table', 'table-striped', 'table-hover', 'table-sm'], bold_rows=False)
                res['занятия/тренеры'] = trainerlessons.to_frame().to_html(header=False,  border=0, classes=['table', 'table-striped', 'table-hover', 'table-sm'], bold_rows=False)
                res['посещения/площадки'] = trainergyms.to_frame().to_html(header=False, border=0, classes=['table', 'table-striped', 'table-hover', 'table-sm'], bold_rows=False)     
            res['Детализация посещений по тренерам'] = lessontypesvisits.to_frame().to_html(header=False, border=0, classes=['table', 'table-striped', 'table-hover', 'table-sm'], bold_rows=False)
            
            res['всего посещений'] = totalvisits
            res['всего занятий'] = totallessons

            # отказываемся от типов билета
            # res['посещения/тип билета'] = totalvisitsbytickettype.to_frame().to_html(header=False, border=0, classes=['table', 'table-striped', 'table-hover', 'table-sm'], bold_rows=False)
            res['занятия/тип занятия'] =  totallessonsbylessntype.to_frame().to_html(header=False, border=0, classes=['table', 'table-striped', 'table-hover', 'table-sm'], bold_rows=False)
            return jsonify(res)
        
        else:
            abort(404)

        
class CompanyAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        # self.reqparse.add_argument('title', type = str, required = True, help = 'No task title provided', location = 'json')
        # self.reqparse.add_argument('description', type = str, default = "", location = 'json')
        self.schema = CompanySchema()
        self.mschema = CompanySchema(many=True)
        self.method_decorators = [auth.login_required]


    #@auth.login_required
    def get(self, id=None):
        if not id:
            companies = Company.query.all()
            return self.mschema.dump(companies).data, 200
        else:
            company = Company.query.filter_by(id=id).first()
            if company:
                return self.schema.dump(company).data, 200

    #@auth.login_required            
    def put(self, id):
        return #update company

    #@auth.login_required
    def post(self):
        return #create company

    #@auth.login_required
    def delete(self, id):
        return #delete company


# /companies/id/staff * * * * * * * * * * * * * * * * * * *
class CompanyStaffAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        # self.reqparse.add_argument('title', type = str, required = True, help = 'No task title provided', location = 'json')
        # self.reqparse.add_argument('description', type = str, default = "", location = 'json')
        self.staffschema = StaffSchema()
        self.mstaffschema = StaffSchema(many=True)
        self.companyschema = CompanySchema()
        self.method_decorators = [auth.login_required]


    #@auth.login_required    
    def get(self, companyid=None, staffid=None):
        if not companyid:
            abort(400, message='No company id provided')
        else:
            company = Company.query.filter_by(id=companyid).first()
            if company:
                if not staffid:
                    return self.companyschema.dump(company).data, 200
                else:
                    staff = Staff.query.join(Company).filter(Company.id == companyid).filter(Staff.id==staffid).first()
                    return self.schema.dump(staff).data, 200
                    

    #@auth.login_required            
    def put(self, id):
        return #update company

    #@auth.login_required
    def post(self):
        return #create company

    #@auth.login_required
    def delete(self, id):
        return #delete company


#/prices
# /gyms * * * * * * * * * * * * * * * * * * *
class PriceAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        # self.reqparse.add_argument('title', type = str, required = True, help = 'No task title provided', location = 'json')
        # self.reqparse.add_argument('description', type = str, default = "", location = 'json')
        self.schema = PriceSchema()
        self.mschema = PriceSchema(many=True)
        self.method_decorators = []

    def options(self, *args, **kwargs):
        return jsonify([])

    @token_required
    @cross_origin()
    def get(self, id=None):
        # only current user's companies
        if not id:
            prices = Price.query.all()
            return jsonify(self.mschema.dump(prices).data), 200
        else:
            price = Price.query.filter_by(id=id).first()
            if price:
                return jsonify(self.schema.dump(price).data), 200

    @token_required
    @cross_origin()
    def patch(self, id):
        attrs = ['pricetype', 'price', 'name', 'numvisits', 'validdays']
        print(request.json)
        price = db.session.query(Price).filter(Price.id==id).first()

        if price:
            for attr in attrs:
                val = request.json.get(attr)
                if attr == 'validdays':
                    try:
                        validdays = int(val)
                    except:
                        validdays = None
                    price.validdays = validdays
                else:
                    setattr(price, attr, val)
                    
            db.session.add(price)
            db.session.commit()
            return jsonify(self.schema.dump(price).data), 201

        abort(404, message="Not  sss")
        
    @token_required
    @cross_origin()
    def post(self):
        print(request.json)
        pricetype = request.json.get('pricetype')
        price = request.json.get('price')
        numvisits = request.json.get('numvisits')
        validdays = request.json.get('validdays')
        
        try:
            price = float(price)
            int(numvisits)
        except:
            abort(400)


        try:
            validdays = int(validdays)
        except:
            validdays = None

            
        name = request.json.get('name')
        newprice = Price(pricetype=pricetype, price=price, name=name, numvisits=numvisits, validdays=validdays)
        
        db.session.add(newprice)
        db.session.commit()
        
        return jsonify(self.schema.dump(newprice).data), 201

    @token_required
    @cross_origin()
    def delete(self, id):
        price = db.session.query(Price).filter(Price.id==id).first()
        if price:
            db.session.delete(price)
            db.session.commit()
            return make_response("Price deleted", 204)
        abort(404, message="Not found")


# /gyms * * * * * * * * * * * * * * * * * * *
class GymAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        # self.reqparse.add_argument('title', type = str, required = True, help = 'No task title provided', location = 'json')
        # self.reqparse.add_argument('description', type = str, default = "", location = 'json')
        self.schema = GymSchema()
        self.mschema = GymSchema(many=True)
        self.method_decorators = []

    def options(self, *args, **kwargs):
        return jsonify([])

    @token_required
    @cross_origin()
    def get(self, id=None):
        # only current user's companies
        if not id:
            gyms = db.session.query(Gym)#Gym.query.all()
            if 'staff' in request.args:
                staffid = request.args.get('staff')
                gyms = gyms.filter(Gym.lessons.any(StudentGroup.trainer_id == staffid))
                print("found trainers")
            if 'strainer' in request.args:
                strainer_id = request.args.get('strainer')
                gyms = gyms.filter(Gym.lessons.any(StudentGroup.strainer_id == strainer_id))
            
            gyms = gyms.all()
            
            return jsonify(self.mschema.dump(gyms).data)
        else:
            gym = Gym.query.filter_by(id=id).first()
            if gym:
                return self.schema.dump(gym).data, 200

    @token_required
    @cross_origin()
    def patch(self, id):
        attrs = ['name', 'address', 'phone', 'company_id', 'color', 'note']
        gym = db.session.query(Gym).filter(Gym.id==id).first()
        if gym:
            for attr in attrs:
                val = request.json.get(attr)
                if attr == 'company_id':
                    company = db.session.query(Company).filter(Company.id==val).first()
                    if company:
                        company.gyms.append(gym)
                        db.session.add(company)
                        db.session.commit()
                else:
                    setattr(gym, attr, val)
            db.session.add(gym)
            db.session.commit()
            return jsonify(self.schema.dump(gym).data), 201

        abort(404, message="Not  found")
        
    @token_required
    @cross_origin()
    def post(self):
        name = request.json.get('name')
        prev_gym = db.session.query(Gym).filter(Gym.name==name).first()
        if prev_gym:
            return abort(409, message="Gym exists")
        address = request.json.get('address')
        phone = request.json.get('phone')
        url = request.json.get('url')
        color = request.json.get('color')
        note = request.json.get('note')
        # DEFAULT COMPANY
        newgym = Gym(name=name, address=address, phone=phone, url=url, company_id = 9, color=color, note=note)
        db.session.add(newgym)
        db.session.commit()
        
        return jsonify(self.schema.dump(newgym).data), 201

    @token_required
    @cross_origin()
    def delete(self, id):
        gym = db.session.query(Gym).filter(Gym.id==id).first()
        if gym:
            db.session.delete(gym)
            db.session.commit()
            return make_response("Gym deleted", 204)
        abort(404, message="Not found")



# /companies/id/gym * * * * * * * * * * * * * * * * * * *
class CompanyGymAPI(Resource):
    """
    Connect companies and gyms
    """
    def __init__(self):
        self.schema = GymSchema()
        self.mschema = GymSchema(many=True)
        self.method_decorators = [auth.login_required]


    #@auth.login_required    
    def get(self, companyid=None,gymid=None):
        if not companyid:
            abort(400, message='No company id provided')
        else:
            company = Company.query.filter_by(id=companyid).first()
            if company:
                if not gymid:
                    return self.mschema.dump(company.gyms).data, 200
                else:
                    gym = Gym.query.join(Company).filter(Company.id == companyid).filter(Gym.id==gymid).first()
                    if gym:
                        return self.schema.dump(gym).data, 200
                    

    #@auth.login_required            
    def put(self, id):
        return #update company

    #@auth.login_required
    def post(self):
        
        return 

    #@auth.login_required
    def delete(self, id):
        return #delete company


        
# /staff * * * * * * * * * * * * * * * * * * *
class StaffAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        # self.reqparse.add_argument('title', type = str, required = True, help = 'No task title provided', location = 'json')
        # self.reqparse.add_argument('description', type = str, default = "", location = 'json')

        self.schema = StaffSchema(exclude=['password_hash',])
        self.m_schema = StaffSchema(many=True, exclude=['password_hash',])
        self.method_decorators = []

        
    def options(self, *args, **kwargs):
        return jsonify([])
        
    ##@auth.login_required
    @token_required
    @cross_origin()
    def get(self, id=None):
        if not id:
            like = request.args.get('like', None)
            stafftype = request.args.get('staff')
            archived = request.args.get('archived', False)
            staff_m = db.session.query(Staff).filter(Staff.archived==False)
            if archived:
                staff_m = db.session.query(Staff).filter(Staff.archived==True)
            if stafftype and stafftype == 'trainers':
                staff_m = staff_m.filter(Staff.trainer == True)
            
            role = request.args.get('role')
            if role and role == 'trainer':
                staff_m = staff_m.filter(Staff.trainer == True)
            if like:
                staff_m = staff_m.filter(Staff.name.ilike("%{}%".format(like.lower())))

            staff_m = staff_m.all()
            print(staff_m)
            return jsonify(self.m_schema.dump(staff_m).data)
        else:
            staff = Staff.query.filter_by(id=id).first()
            if staff:
                return jsonify(self.schema.dump(staff).data), 200
            else:
                abort(404)

    @token_required
    @cross_origin()
    def patch(self, id):
        if not request.json:
            abort(400, message="No data provided")
            
        user = db.session.query(Staff).filter(Staff.id==id).first()
        if user:
            for attr in ['login', 'phone', 'name', 'note', 'admin', 'trainer', 'is_confirmed', 'confirmed_on', 'company_id', 'stafftype', 'grouprate', 'personrate', 'personalrate', 'splitrate', 'minirate', 'password', 'chatid', 'subscribed']:
                val = request.json.get(attr)
                if attr == 'subscribed':
                    if val == 0:
                        user.subscribed = False
                    elif val == 1:
                        user.subscribed = True
                if attr == 'password' and val:
                    #print(user.password_hash)
                    user.hash_password(val)
                    #print(user.password_hash)
                    #db.session.add(user)
                    #db.session.commit()
                    
                elif attr == 'company_id':
                    company = db.session.query(Company).filter(Company.id==val).first()
                    if company:
                        company.staff.append(user)
                        db.session.add(company)
                        db.session.commit()

                elif attr == 'confirmed_on':
                    val = datetime.datetime.now()

                elif attr == 'stafftype':
                    if not val:
                        user.trainer = False
                        user.stafftype = None
                if attr in ['grouprate', 'personrate', 'personalrate', 'splitrate', 'minirate']:

                    if not val:
                        setattr(user, attr, None)
                        
                if val:
                    setattr(user, attr, val)
                
            db.session.add(user)
            db.session.commit()
            return jsonify(self.schema.dump(user).data), 201
        
        abort(404, message="Not found")

    @token_required
    @cross_origin()
    def post(self):
        if not request.json:
            abort(400, message="No data provided")
        login = request.json.get('login')
        phone = request.json.get('phone')
        name = request.json.get('name')
        password = request.json.get('password')
        
        if not(any([login, phone, name])):
            return abort(400, 'Provide required fields for phone, name or login')
        
        prevuser = db.session.query(Staff).filter(Staff.login==login).first()
        if prevuser:
            abort(409, message='User exists')
        note = request.json.get('note')
        isadmin = request.json.get('is_admin')
        if isadmin == '':
            isadmin = False

        is_confirmed = request.json.get('is_confirmed')
        stafftype = request.json.get('stafftype')
        confirmed_on = None
        if is_confirmed:
            confirmed_on = datetime.datetime.today()

        grouprate = request.json.get('grouprate')
        personrate = request.json.get('personrate')
        personalrate = request.json.get('personalrate')
        splitrate = request.json.get('splitrate')
        minirate = request.json.get('minirate')

        #chatid = request.json.get('chatid', None)
        #subscribed = request.json.get('subscribed', None)        
        newuser = Staff(login=login, is_confirmed=is_confirmed, confirmed_on=confirmed_on, phone=phone, name=name, note=note)
        if stafftype:
            newuser.stafftype = stafftype
            newuser.trainer = True
        if grouprate:
            newuser.grouprate = grouprate
        if personrate:
            newuser.personrate = personrate
        if personalrate:
            newuser.personalrate = personalrate
        if splitrate:
            newuser.splitrate = splitrate
        if minirate:
            newuser.minirate = minirate
            
        newuser.admin = isadmin
        
        newuser.hash_password(password)
        db.session.add(newuser)
        db.session.commit()
        
        return jsonify(self.schema.dump(newuser).data), 201
    
    def delete(self, id):
        if not id:
            abort(404, message="Not found")
        user = db.session.query(Staff).filter(Staff.id==id).first()
        if user:
            user.archived = not user.archived
            db.session.add(user)
            db.session.commit()
            return make_response("Staff archived", 204)
        abort(404, message="Not found")

    
# /groups
class StudentGroupAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.schema = StudentGroupSchema()
        self.m_schema = StudentGroupsSchema(many=True, exclude = ("lessons", "students"))
        self.method_decorators = [token_required, cross_origin()]
        
    def options(self, *args, **kwargs):
        return jsonify([])

    @token_required
    @cross_origin()
    def get(self, id=None):
        if not id:
            groups = db.session.query(StudentGroup)#.order_by(StudentGroup.trainer_id)#.all()
            if 'gym' in request.args:
                gym_id = request.args.get('gym')
                groups = groups.filter(StudentGroup.gym_id == gym_id)
            if 'staff' in request.args:
                staff_id = request.args.get('staff')
                groups = groups.filter(StudentGroup.trainer_id == staff_id)
            if 'trainer' in request.args:
                trainer_id = request.args.get('trainer')
                groups = groups.filter(StudentGroup.trainer_id == trainer_id)
            if 'strainer' in request.args:
                strainer_id = request.args.get('strainer')
                groups = groups.filter(StudentGroup.strainer_id == strainer_id)
            print('GET GROUPS', request.args)
            groups = groups.order_by(StudentGroup.trainer_id).all()
            if groups:
                return jsonify(self.m_schema.dump(groups).data)
            else:
                abort(404, message='Not found')
        else:
            group = db.session.query(StudentGroup).filter(StudentGroup.id==id).first()
            if group:
                return jsonify(self.schema.dump(group).data), 200
            else:
                abort(404, message='Not found')

    @token_required
    @cross_origin()
    def patch(self, id):
        group = db.session.query(StudentGroup).filter(StudentGroup.id==id).first()
        name = request.json.get("name")
        minage = request.json.get("minage")
        maxage = request.json.get("maxage")
        trainer_id = request.json.get("trainer_id")
        strainer_id = request.json.get("strainer_id")
        student_id = request.json.get("student_id")
        student_ids = request.json.get("student_ids")
        gym_id = request.json.get("gym_id")
        print(request.json)
        
        if group:
            if gym_id:
                gym = db.session.query(Gym).filter(Gym.id==gym_id).first()
                if gym:
                    group.gym = gym
                
            if trainer_id:
                trainer = db.session.query(Staff).filter(Staff.id==trainer_id).first()
                if trainer:
                    group.trainer = trainer
                    # also should change trainer at all future lessons
                    today = datetime.datetime.today().date()
                    today = datetime.datetime.combine(today, datetime.time(0, 1))
                    lessons = db.session.query(Lesson).filter(Lesson.group == group).filter(Lesson.start >= today).all()

                    if lessons:
                        for lesson in lessons:
                            lesson.trainer = trainer
                            db.session.add(lesson)
                            db.session.commit()
                    
            if strainer_id:
                strainer = db.session.query(Staff).filter(Staff.id==strainer_id).first()
                if strainer:
                    group.strainer = strainer
                    print(group.strainer)
                else:
                    group.strainer = None
                
            if student_id:
                student = db.session.query(Student).filter(Student.id==student_id).first()
                group.students.append(student)
                
                future_lessons = db.session.query(Lesson).filter(Lesson.group == group).filter(Lesson.start >= datetime.date.today()).all()
                for l in future_lessons:
                    if student not in l.students:
                        l.students.append(student)
                        db.session.add(l)
                        db.session.commit()

            if student_ids:
                students = db.session.query(Student).filter(Student.id.in_(student_ids)).all()
                for student in students:
                    if student not in group.students:
                        group.students.append(student)
                        # get group future lessons and add the student to
                        # every lesson
                        # future lessons =
                        future_lessons = db.session.query(Lesson).filter(Lesson.group == group).filter(Lesson.start >= datetime.date.today()).all()
                        for l in future_lessons:
                            if student not in l.students:
                                l.students.append(student)
                                db.session.add(l)
                                db.session.commit()
            if name:
                group.name = name
            if minage:
                group.minage = minage
            if maxage:
                group.maxage = maxage

            db.session.add(group)
            db.session.commit()
            print("COMMIT", group.strainer)
            
            return jsonify(self.schema.dump(group).data), 201

        abort(404, message="Not found")


    @token_required
    @cross_origin()
    def post(self):
        if not request.json:
            abort(400, message="No data provided")

        name = request.json.get('name')
        minage = request.json.get('minage')
        if minage:
            minage = int(minage)

        maxage = request.json.get('maxage')
        if maxage:
            maxage = int(maxage)
            
        trainer_id = request.json.get('trainer_id')
        if trainer_id:
            try:
                trainer_id = int(trainer_id)
            except:
                trainer_id = None

        gym_id = request.json.get('gym_id')
        if gym_id:
            try:
                gym_id = int(gym_id)
            except:
                gym_id = None

                
        strainer_id = request.json.get('strainer_id')
        if strainer_id:
            try:
                strainer_id = int(strainer_id)
            except:
                strainer_id = None
                
        student_ids = request.json.get("student_ids")
        if student_ids:
            students = db.session.query(Student).filter(Student.id.in_(student_ids)).all()
        else:
            students = None
        if name:
            prevgroup = db.session.query(StudentGroup).filter(StudentGroup.name==name).filter(StudentGroup.trainer_id==trainer_id).filter(StudentGroup.gym_id == gym_id).first()
            if not prevgroup:
                newgroup = StudentGroup(name=name, minage=minage, maxage=maxage)
                if trainer_id:
                    trainer = Staff.query.filter(Staff.id == trainer_id).first()
                    if trainer:
                        newgroup.trainer = trainer
                    strainer = Staff.query.filter(Staff.id == strainer_id).first()
                    if strainer:
                        newgroup.strainer = strainer
                    if students:
                        newgroup.students = students
                    if gym_id:
                        gym = Gym.query.filter(Gym.id == gym_id).first()
                    if gym:
                        newgroup.gym = gym

                db.session.add(newgroup)
                db.session.commit()
                
                return jsonify(self.schema.dump(newgroup).data), 200
            else:
                return abort(409, message='Resource exists')
        else:
            abort(400, message="No data provided")

    @token_required
    @cross_origin()
    def delete(self, id):
        if id:
            group = db.session.query(StudentGroup).filter(StudentGroup.id==id).first()
            if group:
                db.session.delete(group)
                db.session.commit()
                return make_response('Group deleted', 204)
        abort(404, "Not found")
        
            
class LessonStudentAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        # self.reqparse.add_argument('title', type = str, required = True, help = 'No task title provided', location = 'json')
        # self.reqparse.add_argument('description', type = str, default = "", location = 'json')
        self.schema = StudentSchema()
        self.m_schema = StudentSchema(many=True)
        self.method_decorators = []

        
    def options(self, *args, **kwargs):
        return jsonify([])

    @token_required
    @cross_origin()
    def get(self, lessonid=None, studid=None):
        if not lessonid:
            return abort(400, message='no id specified')
        else:
            lesson = Lesson.query(id=groupid).first()
            if lesson:
                if not studid:
                    return jsonify(self.m_schema.dump(group.students).data), 200
                else:
                    student = Student.query.join(Lesson).filter(Lesson.id == lessonid).filter(Student.id==studid).first()
                    if student:
                        return jsonify(self.schema.dump(student).data), 200
            return # single stgroup

    def put(self, id):
        return #update stgroup

    @token_required
    @cross_origin()
    def post(self, lessonid):
        return #create stgroup
    
    @token_required
    @cross_origin()
    def delete(self, lessonid, studid):
        student = db.session.query(Student).filter(Student.id==studid).first()
        lesson = db.session.query(Lesson).filter(Lesson.id==lessonid).first()
        if lesson and student:
            if student in lesson.students:
                lesson.students.remove(student)
                db.session.commit()
                return make_response('Student deleted from lesson', 204)
            else:
                abort(404)

# /stgroup/id/student
class StudentGroupStudentAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        # self.reqparse.add_argument('title', type = str, required = True, help = 'No task title provided', location = 'json')
        # self.reqparse.add_argument('description', type = str, default = "", location = 'json')
        self.schema = StudentSchema()
        self.m_schema = StudentSchema(many=True)
        self.method_decorators = []

    def options(self, *args, **kwargs):
        return jsonify([])

    @token_required
    @cross_origin()
    def get(self, groupid=None, studid=None):
        if not groupid:
            return abort(400, message='no id specified')
        else:
            group = StudentGroup.query(id=groupid).first()
            if group:
                if not studid:
                    return self.mschema.dump(group.students).data, 200
                else:
                    student = Student.query.join(StudentGroup).filter(StudentGroup.id == groupid).filter(Student.id==studid).first()
                    if student:
                        return self.schema.dump(student).data, 200
            return # single stgroup

    def put(self, id):
        return #update stgroup

    @token_required
    @cross_origin()
    def post(self):
        return #create stgroup
    
    @token_required
    @cross_origin()
    def delete(self, groupid, studid):
        student = db.session.query(Student).filter(Student.id==studid).first()
        group = db.session.query(StudentGroup).filter(StudentGroup.id==groupid).first()
        if group and student:
            group.students.remove(student)
            future_lessons = db.session.query(Lesson).filter(Lesson.group == group).filter(Lesson.start >= datetime.date.today()).all()
            for l in future_lessons:
                if student in l.students:
                    l.students.remove(student)
                    db.session.commit()
            #db.session.add(group)
            db.session.commit()
            return make_response('Student deleted from group', 204)
    


# /stgroup/id/trainer
class StudentGroupStaffAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        # self.reqparse.add_argument('title', type = str, required = True, help = 'No task title provided', location = 'json')
        # self.reqparse.add_argument('description', type = str, default = "", location = 'json')
        self.schema = Schema()
        self.m_schema = StudentSchema(many=True)
        self.method_decorators = [auth.login_required]
    
    def get(self, groupid=None, staffid=None):
        if not groupid:
            return abort(400, message='no id specified')
        else:
            group = StudentGroup.query(id=groupid).first()
            if group:
                if not staffid:
                    return self.mschema.dump(group.staff).data, 200
                else:
                    staff = Staff.query.join(StudentGroup).filter(StudentGroup.id == groupid).filter(Staff.id==staff).first()
                    if staff:
                        return self.schema.dump(staff).data, 200
            return # single stgroup

    def put(self, id):
        return #update stgroup

    def post(self):
        return #create stgroup

    def delete(self, id):
        return #delete stgroup
    

class StudentAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        # self.reqparse.add_argument('title', type = str, required = True, help = 'No task title provided', location = 'json')
        # self.sreqparse.add_argument('description', type = str, default = "", location = 'json')
        self.schema = StudentSchema()
        self.m_schema = StudentSchema(many=True, exclude=["lessons", "tickets", "visit"])
        self.method_decorators = []
        
    def options(self, *args, **kwargs):
        return jsonify([])
        
    @token_required
    @cross_origin()
    def get(self, id=None):
        if not id:
            students = db.session.query(Student)
            print("SEARCH STUDENTS")
            print(request.args)
            if 'query' in request.args:
                q = request.args.get('query')
                term = "%{}%".format(q)
                students = students.filter(or_(Student.name.ilike(term), Student.email.ilike(term), Student.phone.ilike(term)))
                lesson_id = request.args.get('lesson_id')
                if lesson_id is not None:
                    students = students.filter(~Student.lessons.any(Lesson.id == lesson_id))
            if 'group' in request.args:
                group_id = request.args.get('group')
                if group_id != "":
                    students = students.filter(Student.studentgroup.any(StudentGroup.id == group_id))
            if 'gender' in request.args:
                gender = request.args.get('gender')
                if gender != '':
                    students = students.filter(Student.gender == gender)
            if 'age' in request.args:
                age = request.args.get('age')
                if age != "":
                    age = int(age)
                    print("AGE", age)
                    # calculate bdate
                    # today
                    today = datetime.datetime.now().date()
                    ago = today - relativedelta(years=age)
                    prevyear = today - relativedelta(years=age+1)
                    students = students.filter(Student.bdate <= ago).filter(Student.bdate >= prevyear)
            if 'grade' in request.args:
                grade = request.args.get('grade')
                students = students.filter(Student.grade == grade)
            if 'contractnum' in request.args:
                contractnum = request.args.get('contractnum')
                if contractnum != "":
                    students = students.filter(Student.contract_id == contractnum)
            if 'visits' in request.args:
                visits = request.args.get('visits')
                filter_after = datetime.datetime.today() - datetime.timedelta(days = 10)
                if visits == 'frequent':
                    students = students.join(LessonVisit).filter(LessonVisit.datetime >= filter_after )
                else:
                    students = students.join(LessonVisit).filter(LessonVisit.datetime < filter_after )
                    
            if 'students_id' in request.args:
                stids = request.args.get('students_id')
                if stids:
                    students_ids = json.loads(stids)
                    students = students.filter(Student.id.in_(students_ids))
            if 'archived' in request.args:
                students = students.filter(Student.is_deleted == 1)
            else:
                students = students.filter(Student.is_deleted == 0)
                    
            students = students.all()
            if not students:
                abort(404)

            return jsonify(self.m_schema.dump(students).data)
        else:
            student = Student.query.filter_by(id=id).first()
            if student:
                return jsonify(self.schema.dump(student).data), 200
            else:
                abort(404)
                
    @token_required
    @cross_origin()
    def patch(self, id):
        """
        Edit Student properties,
        add / delete group
        AND all future lessons for the student
        """

        attrs = ['name', 'address', 'phone', 'bdate', 'gender', 'email', 'note', 'is_confirmed', 'group_ids', 'parent_phone', 'parent_name', 'parent_bdate', 'parent_email', 'contract_id', 'is_deleted', 'grade']
        student = db.session.query(Student).filter(Student.id==id).first()

        if student:
            for attr in attrs:
                val = request.json.get(attr)
                if attr == 'is_confirmed' and attr in request.json:
                    student.is_confirmed = val
                    student.confirmed_on = datetime.datetime.now()
                elif attr == 'is_deleted' and attr in request.json:
                    student.is_deleted = val
                elif attr == 'group_ids':
                    # remove from prev_groups

                    prev_groups = request.json.get("prev_groups")
                    if prev_groups:
                        for prev_gr in prev_groups:
                            if prev_gr not in val:
                                # delete from group
                                prevgroup = db.session.query(StudentGroup).filter(StudentGroup.id == prev_gr).first()
                                if prevgroup:
                                    if student in prevgroup.students:
                                        prevgroup.students.remove(student)
                                        db.session.add(prevgroup)
                                        db.session.commit()
                                        pr_future_lessons = db.session.query(Lesson).filter(Lesson.group == prevgroup).filter(Lesson.start >= datetime.date.today()).all()
                                    for l in pr_future_lessons:
                                        if student  in l.students:
                                            l.students.remove(student)
                                            db.session.add(l)
                                            db.session.commit()
                    if val:
                        for gr in val:
                            group = db.session.query(StudentGroup).filter(StudentGroup.id == gr).first()
                            if group:
                                if student not in group.students:
                                    group.students.append(student)
                                    db.session.add(group)
                                    db.session.commit()
                                    future_lessons = db.session.query(Lesson).filter(Lesson.group == group).filter(Lesson.start >= datetime.date.today()).all()
                                    for l in future_lessons:
                                        if student not in l.students:
                                            l.students.append(student)
                                            db.session.add(l)
                                            db.session.commit()
                else:
                    val = request.json.get(attr)
                    if val is not None:
                        setattr(student, attr, val)
                        
            db.session.add(student)
            db.session.commit()
            return jsonify(self.schema.dump(student).data), 201
        abort(404, message="Not found")

    @token_required
    @cross_origin()
    def post(self):
        if not request.json:
            abort(400, message="No data provided")
        name = request.json.get('name')
        address = request.json.get('address')
        phone = request.json.get('phone')
        email = request.json.get('email')
        gender = request.json.get('gender')
        grade = request.json.get('grade')
        bdate = request.json.get('bdate')
        note = request.json.get('note')
        staff_id = request.json.get('staff_id')
        contract_id = request.json.get('contract_id')
        if contract_id:
            try:
                contract_id = int(contract_id)
            except:
                contract_id = None
                
        prev_student = db.session.query(Student).filter(Student.name.ilike("%{}%".format(name))).first()
        if prev_student:
            return abort(409, message="Student exists")
        
        staff = db.session.query(Staff).filter(Staff.id==staff_id).first()
        
        newstudent = Student(name=name, address=address, phone=phone, gender=gender, email=email, note=note, contract_id=contract_id)
        if bdate:
            newstudent.bdate = bdate
        if staff:
            newstudent.added_by = staff
            
        db.session.add(newstudent)
        db.session.commit()
        
        return jsonify(self.schema.dump(newstudent).data), 201

    @token_required
    @cross_origin()
    def delete(self, id):
        student = db.session.query(Student).filter(Student.id==id).first()
        # delete student visits before:
        db.session.query(LessonVisit).filter(LessonVisit.student==student).delete()
        db.session.commit()
        
        if student:
            student.is_deleted = True
            db.session.add(student)
            db.session.commit()
            return make_response('Student deleted', 204)
        abort(404, message="Not found")


class LessonAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.schema = LessonSchema()
        self.m_schema = LessonsSchema(many=True)

    def options(self, *args, **kwargs):
        return jsonify([])

    @token_required
    @cross_origin()
    def get(self, id=None):
        if not id:
            lessons = Lesson.query
            daysfrom = request.args.get('from')
            daysto = request.args.get('to')
            excl = request.args.get('excl')

            if daysto and daysto not in ['yesterday', 'today', 'tomorrow']:
                daysto = datetime.datetime.strptime(daysto, "%Y-%m-%d").date()
                daysto = datetime.datetime.combine(daysto, datetime.time(23, 59))
            if daysfrom and daysfrom not in ['yesterday', 'today', 'tomorrow']:
                daysfrom = datetime.datetime.strptime(daysfrom, "%Y-%m-%d").date()
                if daysto == '':
                    daysto = datetime.datetime.combine(daysfrom, datetime.time(23, 59))
                daysfrom = datetime.datetime.combine(daysfrom, datetime.time(0, 1))
            
                #if not daysto:
                #    daysto = daysfrom
            
            limit = request.args.get('limit')
            staff_id = request.args.get('staff_id')
            strainer_id = request.args.get('strainer_id')
            gym_id = request.args.get('gym_id')
            timefrom = request.args.get('timefrom')
            lessontype = request.args.get('lessontype')
            byday = request.args.get('byday')
            query = request.args.get('query')
            group_id = request.args.get('group_id')
            print(request.args)
            if group_id:
                group = db.session.query(StudentGroup).filter(StudentGroup.id == group_id).first()
                if group:
                    lessons = lessons.filter(Lesson.group == group)
            if gym_id:
                gym = db.session.query(Gym).filter(Gym.id == gym_id).first()
                if gym:
                    lessons = lessons.filter(Lesson.gym_id == gym_id)

            if lessontype:
                lessons = lessons.filter(Lesson.lessontype == lessontype)
                
            if query:
                print("Student query")
                term = "%{}%".format(query)
                lessons = lessons.filter(Lesson.students.any(or_(Student.name.ilike(term), Student.email.ilike(term), Student.phone.ilike(term))))
                
            if not any([staff_id, strainer_id]):
                if daysfrom == 'today':
                    print(111)
                    lessons = lessons.filter(Lesson.start >= datetime.date.today())
                elif daysfrom == 'tomorrow':
                    print(222)
                    lessons = lessons.filter(Lesson.start > datetime.date.today())
                elif daysfrom == 'yesterday':
                    print(333)
                    ydate = datetime.date.today() - datetime.timedelta(1)
                    lessons = lessons.filter(Lesson.start >= ydate)
                else:

                    if daysfrom:
                        lessons = lessons.filter(Lesson.start >= daysfrom)
                        if daysto == '':
                            lessons = lessons.filter(Lesson.end <= daysfrom)
                    if daysto and daysto != '':
                        lessons = lessons.filter(Lesson.end <= daysto)
                        
                        
                if limit:
                    print(666)
                    if limit == "monthend":
                        print(777)
                        ldate = lastdate()
                        print(ldate)
                        ldate = datetime.datetime.combine(ldate, datetime.time(23, 59))
                        lessons = lessons.filter(Lesson.end <= ldate)
                        for l in lessons.order_by(Lesson.start).all():
                            print(l.start, l.end)
                        
                    else:
                        print(888)
                        if daysfrom and not daysto:
                            nextdate = daysfrom + datetime.timedelta(days=int(limit))
                        elif not daysfrom and not daysto:
                            nextdate = datetime.datetime.now() + datetime.timedelta(days=int(limit))
                        lessons = lessons.filter(Lesson.start <= nextdate)

                    
            else:
                reslist = []

                if daysfrom not in ['yesterday', 'today', 'tomorrow']:
                    fromdate = daysfrom#datetime.datetime.strptime(daysfrom, "%Y-%m-%d").date()
                else:
                    
                    if daysfrom == 'today':
                        fromdate = datetime.date.today()
                    elif daysfrom == 'yesterday':
                        fromdate = datetime.date.today() - datetime.timedelta(1)
                    elif  daysfrom == 'tomorrow':
                        fromdate = datetime.date.today() + datetime.timedelta(1)
                            
                    if timefrom:
                        tfrom =  datetime.datetime.strptime(timefrom, '%H:%M').time()
                        fromdate = datetime.datetime.combine(fromdate, tfrom)
                if daysto:
                    nextdate = daysto
                    
                if limit == 'monthend':
                    print(112)
                    nextdate = lastdate()
                else:
                    if limit:
                        nextdate = fromdate + datetime.timedelta(days=int(limit))
                        print(117, type(fromdate), type(nextdate))
                    #if timefrom:
                    #    nextdate = nextdate.date()

                # for trainer's timetable
                if not group_id:
                    if staff_id:
                        lessons = lessons.filter(or_(Lesson.trainer_id==staff_id, Lesson.strainer_id==staff_id))
                    if strainer_id:
                       lessons = lessons.filter(Lesson.strainer_id==strainer_id)
                if isinstance(fromdate, datetime.date):
                    if timefrom:
                        tfrom =  datetime.datetime.strptime(timefrom, '%H:%M').time()
                        fromdate = datetime.datetime.combine(fromdate, tfrom)
                    else:

                        fromdate = datetime.datetime.combine(fromdate, datetime.time(0, 1))
                    if not limit and not daysto:
                        nextdate = datetime.datetime.combine(fromdate, datetime.time(23, 59))
                if fromdate and nextdate:
                    lessons = lessons.filter(Lesson.start >= fromdate).filter(Lesson.start <= nextdate).order_by(Lesson.start.asc())
                
                if gym_id:
                    lessons = lessons.filter(Lesson.gym_id == gym_id)

                if lessontype:
                    lessons = lessons.filter(Lesson.lessontype == lessontype)

            if excl:
                lessons = lessons.filter(Lesson.lessontype != excl)
            
            lessons = lessons.all()
                
            if lessons:
                if byday:
                    res = {}
                    for l in lessons:
                        ldate = str(l.start.date())
                        if ldate not in res.keys():
                            res[ldate] = [l,]
                        else:
                            res[ldate].append(l)
                    for k in res.keys():
                        res[k] = self.m_schema.dump(res[k]).data
                    # print(321, res)
                    return jsonify(res), 200

                return jsonify(self.m_schema.dump(lessons).data), 200
            else:
                abort(404)
        else:
            lesson = Lesson.query.filter_by(id=id).first()
            if lesson:
                return jsonify(self.schema.dump(lesson).data), 200
            
        abort(404, message="No lesson found")

    @token_required
    @cross_origin()
    def patch(self, id):
        lesson = db.session.query(Lesson).filter(Lesson.id==id).first()
        if lesson:
            print(request.json)
            gym_id = request.json.get('gym_id')
            group_id = request.json.get('group_id')
            trainer_id = request.json.get('trainer_id')
            strainer_id = request.json.get('strainer_id')
            date = request.json.get('date')
            note = request.json.get('note')
            start = request.json.get('start')
            end = request.json.get('end')
            student_ids = request.json.get("student_ids")
            lessontype = request.json.get("lessontype")
            print('STRAINER', strainer_id)
            if trainer_id:
                trainer = db.session.query(Staff).filter(Staff.id==trainer_id).first()
                if trainer:
                    lesson.trainer = trainer
                    
            if strainer_id:
                strainer = db.session.query(Staff).filter(Staff.id==strainer_id).first()
                if strainer:
                    lesson.strainer = strainer
            else:
                lesson.strainer = None
                
            if student_ids:
                students = db.session.query(Student).filter(Student.id.in_(student_ids)).all()
                if students:
                    for s in students:
                        if s not in lesson.students:
                            lesson.students.append(s)
                db.session.add(lesson)
                db.session.commit()
                
            gym = db.session.query(Gym).filter(Gym.id == gym_id).first()
            group = db.session.query(StudentGroup).filter(StudentGroup.id == group_id).first()

            if start is not None:
                lesson.start = start
            if end is not None:
                lesson.end = end
            if note is not None:
                lesson.note = note
                
            if group is not None:
                if lesson.group != group:
                    lesson.group = group
                    # добавляем всех студентов в урок
                    for student in group.students:
                        if student not in lesson.students:
                            lesson.students.append(student)
                            db.session.commit()
                
            if gym is not None:
                lesson.gym = gym
            
            if lessontype is not None:
                lesson.lessontype = lessontype
                
            db.session.add(lesson)
            db.session.commit()
            return jsonify(self.schema.dump(lesson).data), 201
        
        abort(404, message="Lesson not found")

    @token_required
    @cross_origin()
    def post(self):
        print(request.json)
        gym_id = request.json.get('gym_id')
        group_id = request.json.get('group_id')
        date = request.json.get('date')
        lesson_id = request.json.get('lesson_id')
        trainer_id = request.json.get('trainer_id')
        strainer_id = request.json.get('strainer_id')
        lessontype = request.json.get("lessontype")
        note = request.json.get('note')
        start = request.json.get('start')
        end = request.json.get('end')
        gym = db.session.query(Gym).filter(Gym.id == gym_id).first()
        if group_id:
            group = db.session.query(StudentGroup).filter(StudentGroup.id == group_id).first()

        if gym:
            #if training_day:
            prev_lesson = db.session.query(Lesson).filter(Lesson.id==lesson_id).first()
            if prev_lesson:
                abort(409, message="Lesson exists")
            #else:
                #training_day = TrainingDay(date=date)
                #db.session.add(training_day)
                #db.session.commit()
                
            newlesson = Lesson(
                note=note,
                start=start,
                end=end,
                #training_day_id=training_day.id,
                gym=gym,
                #group=group
            )
            if group_id:
                newlesson.group = group
                
            if trainer_id:
                trainer = db.session.query(Staff).filter(Staff.id==trainer_id).first()
                if trainer:
                    newlesson.trainer = trainer
            if strainer_id:
                strainer = db.session.query(Staff).filter(Staff.id==strainer_id).first()
                if strainer:
                    newlesson.strainer = strainer
                    
            if lessontype:
                newlesson.lessontype = lessontype
                
            if group_id:
                for s in group.students:
                    if s not in newlesson.students:
                        newlesson.students.append(s)
                        db.session.commit()
            if trainer.chatid and trainer.subscribed:
                ltype = transl_enum[newlesson.lessontype]
                MESSAGE = f"Добавлено новое занятие, {trainer.name}, {ltype}, {gym.name}, {start}, {end}"
                requests.get("https://api.telegram.org/bot{}/sendMessage?chat_id={}&text={}".format(BOT_KEY, trainer.chatid, MESSAGE), proxies=dict(http='socks4://localhost:9050', https='socks4://localhost:9050'))
            # chat id would be stored in the trainer's profile and updated by the teoegram bot @o_skal_bot
            db.session.add(newlesson)
            db.session.commit()
            return jsonify(self.schema.dump(newlesson).data), 201
        
        abort(400, message="Not enough data")

    @token_required
    @cross_origin()
    def delete(self, id):
        lesson = db.session.query(Lesson).filter(Lesson.id==id).first()
        if lesson:
            db.session.delete(lesson)
            db.session.commit()
            return make_response("Lesson deleted", 204)
        abort(404, message="Lesson not found")



class LessonVisitAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.schema = LessonVisitSchema()
        self.m_schema = LessonVisitSchema(many=True)
        self.method_decorators = []

    def options(self, *args, **kwargs):
        return jsonify([])

    @token_required
    @cross_origin()
    def get(self, id=None):
        if not id:
            show_cancelled = False
            
            if request.args.get('show_cancelled') == 'true':
                show_cancelled = True
                
            if show_cancelled:
                visits = db.session.query(LessonVisit)
            else:
                visits = db.session.query(LessonVisit).filter(LessonVisit.visited)
                
            if 'lesson_id' in request.args:
                lesson_id = int(request.args.get("lesson_id"))
                visits = visits.filter(LessonVisit.lesson.has(Lesson.id == int(lesson_id)))
                print(111)
                
            if 'student_id' in request.args:
                student_id = int(request.args.get("student_id"))
                visits = visits.filter(LessonVisit.student.has(Student.id == int(student_id)))
                print(222)
                
            if 'staff_id' in request.args:
                staff_id = int(request.args.get("staff_id"))
                visits = visits.filter(LessonVisit.lesson.has(Lesson.trainer_id == int(staff_id)))
                print(333)
            
            if 'gym_id' in request.args:
                gym_id = int(request.args.get("gym_id"))
                visits = visits.filter(LessonVisit.lesson.has(Lesson.gym_id == int(gym_id)))
                print(333)

            if 'query' in request.args:
                q = request.args.get('query')
                term = "%{}%".format(q)
                visits = visits.filter(LessonVisit.student.has(or_(Student.name.ilike(term), Student.email.ilike(term), Student.phone.ilike(term))))
                print(444)
                
            if 'group' in request.args:
                group_id = request.args.get('group')
                stgroup = db.session.query(StudentGroup).filter(StudentGroup.id == group_id).first()
                visits = visits.filter(LessonVisit.lesson.has(Lesson.group == stgroup))
                print(555)
                
            if 'from' in request.args:
                 
                fr = request.args.get('from')
                fr = datetime.datetime.strptime(fr, "%Y-%m-%d").date()
                fr = datetime.datetime.combine(fr, datetime.time(0, 1))
                visits = visits.filter(LessonVisit.lesson.has(Lesson.start >= fr))
                print(fr)
                
            if 'to' in request.args:
                to = request.args.get('to')
                to = datetime.datetime.strptime(to, "%Y-%m-%d").date()
                to = datetime.datetime.combine(to, datetime.time(23, 59))

                visits = visits.filter(LessonVisit.lesson.has(Lesson.start <= to))

                print(to)
                
            visits = visits.all()
            
            if visits:
                return jsonify(self.m_schema.dump(visits).data), 200
            else:
                abort(404)
        else:
            visit = LessonVisit.query.filter_by(id=id).first()
            if visit:
                return jsonify(self.schema.dump(visit).data), 200

            abort(404)

    @token_required
    @cross_origin()
    def patch(self, id):
        visit = db.session.query(LessonVisit).filter(LessonVisit.id==id).first()
        if not visit:
            abort(404, message="Visit not found")

        visited = request.json.get("visited")
        cancelled = request.json.get("cancelled")
        note = request.json.get("note")
        visittype = request.json.get("visittype")
        staff_id = request.json.get("staff_id")

        staff = db.session.query(Staff).filter(Staff.id == int(staff_id)).first()
        
        #if staff:
        if staff:
            visit.edited_by = staff

        if visited:
            visit.visited = visited
        else:
            visit.visited = False
            
        if cancelled:
            visit.cancelled = cancelled
        else:
            visit.cancelled = False
            
        if visittype:
            visit.visittype = visittype
            
        visit.lessontype = visit.lesson.lessontype
        #if 'visited' in request.json and not visited:
        #    visit.cancelled = True
        
        if note:
            visit.note = note

        visit.edited = datetime.datetime.now()
        db.session.add(visit)
        db.session.commit()

        #student = visit.student
        #tkt = student.lastvalidticket
        #if visited:
        #    tkt.visitsleft = tkt.visitsleft - 1
        # снимаем посещение только в POST
        
        #db.session.add(tkt)
        return jsonify(self.schema.dump(visit).data), 201

    @token_required
    @cross_origin()
    def post(self):
        lesson_id = request.json.get("lesson_id")
        student_id = request.json.get("student_id")
        note = request.json.get("note")
        staff_id = request.json.get("staff_id")
        
        prev_visit = db.session.query(LessonVisit).filter(LessonVisit.lesson_id==lesson_id).filter(LessonVisit.student_id==student_id).first()
        if prev_visit:
            abort(409, message="Visit exists")
            
        student = db.session.query(Student).filter(Student.id==student_id).first()
        lesson = db.session.query(Lesson).filter(Lesson.id==lesson_id).first()
        if student:
            # which ticket to choose?
            # lastvalid? 
            #print('LASTVALIDTICKET', student.lastvalidticket.id)
            # print("OLOLO")
            #>>>
            # select last valid ticket and add it to visit to
            # track which ticket was used??
            # or one student - one ticket? one VALID ticket
            new_visit = LessonVisit(lesson_id=lesson_id, student_id=student_id, note=note)
            new_visit.lessontype = lesson.lessontype
            new_visit.datetime = datetime.datetime.now()
            if staff_id:
                staff = db.session.query(Staff).filter(Staff.id == staff_id).first()
                if staff:
                    new_visit.edited_by = staff

            new_visit.edited = datetime.datetime.now()
            
            # db.session.add(new_visit)
            # db.session.commit()
            # subtract one visit from student ticket
            tkt = student.lastvalidticket

            if tkt:
                tkt.visitsleft = tkt.visitsleft - 1
                new_visit.ticket = tkt
            else:
                price = db.session.query(Price).filter(Price.name=='долг').first()
                tkt = Ticket(student=student, note=note, price=price, visitsleft=1, cancels=0, transfers=0, is_paid=True)
                db.session.add(tkt)
                new_visit.ticket = tkt

            db.session.add(tkt)
            db.session.add(new_visit)
            db.session.commit()

        return jsonify(self.schema.dump(new_visit).data), 201

    @token_required
    @cross_origin()
    def delete(self, id):
        return #delete day


class TicketAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        # self.reqparse.add_argument('title', type = str, required = True, help = 'No task title provided', location = 'json')
        # self.reqparse.add_argument('description', type = str, default = "", location = 'json')
        self.schema = TicketSchema()
        self.m_schema = TicketSchema(many=True)
        self.method_decorators = []

    def options(self, *args, **kwargs):
        return jsonify([])

    @token_required
    @cross_origin()
    def get(self, id=None):
        if not id:
            tickets = Ticket.query.all()
            if tickets:
                return jsonify(self.m_schema.dump(tickets).data), 200
            else:
                return jsonify([]), 200
        else:
            ticket = Ticket.query.filter_by(id=id).first()
            if ticket:
                return jsonify(self.schema.dump(ticket).data), 200
            else:
                abort(404)
                
    @token_required
    @cross_origin()
    def patch(self, id):
        print(request.json)
        ticket = db.session.query(Ticket).filter(Ticket.id==id).first()
        if ticket:
            student_id = request.json.get("student_id")
            student = db.session.query(Student).filter(Student.id==student_id).first()
            if student:
                note = request.json.get("note")
                try:
                    cancels = int(request.json.get("cancels"))
                    visitsleft = int(request.json.get("visitsleft"))
                    transfers = int(request.json.get("transfers"))
                except:
                    abort(400)
                
            price_id = request.json.get("price_id")
            is_paid = request.json.get("is_paid")
            price = db.session.query(Price).filter(Price.id==price_id).first()
            if student and price:
                ticket.is_paid = is_paid
                ticket.student = student
                ticket.price = price
                ticket.cancels = cancels
                ticket.transfers = transfers
                ticket.note = note
                ticket.visitsleft = visitsleft
                if price.validdays:
                    today = datetime.datetime.now()
                    enddate = today + datetime.timedelta(days=price.validdays)
                    ticket.ends = enddate.date()
                else:
                    lastday = lastdate()
                    print("LASTDAY", lastday)
                    ticket.ends = lastday
                db.session.add(ticket)
                db.session.commit()
                return jsonify(self.schema.dump(ticket).data), 201

            else:
                if 'transfers' in request.json:
                    try:
                        transfers = int(request.json.get("transfers"))
                    except:
                        abort(400)
                    if transfers:
                        ticket.transfers = transfers
                if 'cancels' in request.json:
                    try:
                        cancels = int(request.json.get("cancels"))
                    except:
                        abort(404)
                    if cancels:
                        ticket.cancels = cancels
                    
                db.session.add(ticket)
                db.session.commit()
                return jsonify(self.schema.dump(ticket).data), 201


    @token_required
    @cross_origin()
    def post(self):
        student_id = request.json.get("student_id")
        student = db.session.query(Student).filter(Student.id==student_id).first()
        if student:
            note = request.json.get("note")
            try:
                cancels = int(request.json.get("cancels"))
                visitsleft = int(request.json.get("visitsleft"))
                transfers = int(request.json.get("transfers"))
            except:
                abort(400)
                
            price_id = request.json.get("price_id")
            is_paid = request.json.get("is_paid")
            price = db.session.query(Price).filter(Price.id==price_id).first()
        if student and price:
            # cancellling all previous tickets
            for t in student.validtickets:
                t.is_cancelled = True
                db.session.add(t)
                db.session.commit()

            newticket = Ticket(student=student, note=note, price=price, visitsleft=visitsleft, cancels=cancels, transfers=transfers, is_paid=is_paid)
            if price.validdays:
                today = datetime.datetime.now()
                enddate = today + datetime.timedelta(days=price.validdays)
                newticket.ends = enddate.date()
                
            db.session.add(newticket)
            db.session.commit()
            return jsonify(self.schema.dump(newticket).data), 200
        
        abort(404)
        
    @token_required
    @cross_origin()
    def delete(self, id):
        ticket = Ticket.query.filter_by(id=id).first()
        if ticket:
            if ticket.is_cancelled:
                # cancel other tickets
                student = ticket.student
                for t in student.validtickets:
                    t.is_cancelled = True
                    db.session.add(t)
                    db.session.commit()
                    
                ticket.is_cancelled = False
            else:
                ticket.is_cancelled = True

                
            db.session.add(ticket)
            db.session.commit()
            return make_response("Ticket deleted", 204)
        abort(404)

# R E L A T I O N S
# +/companies/1/gyms
# +/companies/1/staff
# +/groups/1/students
# +/groups/1/trainers

# >>>>> STOPPED HERE
# /days/1/lessons
# /lessons/1/visits
# /visits/1/students
# /students/1/tickets

# How to optimize?
# >>> Stopped here
# Arrange classes, Schemas -> Single API -> Multiple API
# TODO add GET POST PATCH DELETE
# Write requests - what should we do?
api.add_resource(StatsAPI, '/stats', '/stats/<int:id>', endpoint = 'stats')
api.add_resource(StatsDetailsAPI, '/statsdetails', endpoint = 'statsdetails')
api.add_resource(ImportsAPI, '/imports', endpoint='imports')

api.add_resource(PriceAPI, '/prices/<int:id>', endpoint = 'price')
api.add_resource(PriceAPI, '/prices', endpoint = 'prices')
api.add_resource(GymAPI, '/gyms/<int:id>', endpoint = 'gym')
api.add_resource(GymAPI, '/gyms', endpoint = 'gyms')
api.add_resource(StaffAPI, '/staff/<int:id>', endpoint = 'staff')
api.add_resource(StaffAPI, '/staff', endpoint = 'staffm')
#api.add_resource(TrainingDayAPI, '/days/<int:id>', endpoint = 'day')
#api.add_resource(TrainingDayAPI, '/days', endpoint = 'days')

api.add_resource(LessonAPI, '/lessons/<int:id>', endpoint = 'lesson')
api.add_resource(LessonAPI, '/lessons', endpoint = 'lessons')
api.add_resource(LessonVisitAPI, '/visits/<int:id>', endpoint = 'visit')
api.add_resource(LessonVisitAPI, '/visits', endpoint = 'visits')
api.add_resource(TicketAPI, '/tickets/<int:id>', endpoint = 'ticket')
api.add_resource(TicketAPI, '/tickets', endpoint = 'tickets')
## RELATIONS
api.add_resource(CompanyAPI, '/companies/<int:id>', endpoint = 'company')
api.add_resource(CompanyAPI, '/companies', endpoint = 'companies')

api.add_resource(CompanyStaffAPI, '/companies/<int:companyid>/staff', endpoint = 'companystaffs')
api.add_resource(CompanyStaffAPI, '/companies/<int:companyid>/staff/<int:staffid>', endpoint = 'companystaff')
api.add_resource(CompanyGymAPI, '/companies/<int:companyid>/gyms', endpoint = 'companygyms')
api.add_resource(CompanyGymAPI, '/companies/<int:companyid>/gyms/<int:gymid>', endpoint = 'companygym')
api.add_resource(StudentAPI, '/students/<int:id>', endpoint = 'student')
api.add_resource(StudentAPI, '/students', endpoint = 'students')
api.add_resource(StudentGroupAPI, '/groups/<int:id>', endpoint = 'group')
api.add_resource(StudentGroupAPI, '/groups', endpoint = 'groups')
api.add_resource(LessonStudentAPI, '/lessons/<int:lessonid>/students', endpoint = 'lessonstudents')
api.add_resource(LessonStudentAPI, '/lessons/<int:lessonid>/students/<int:studid>', endpoint = 'lessonstudent')
api.add_resource(StudentGroupStudentAPI, '/groups/<int:groupid>/students', endpoint = 'groupstudents')
api.add_resource(StudentGroupStudentAPI, '/groups/<int:groupid>/students/<int:studid>', endpoint = 'groupstudent')
api.add_resource(StudentGroupStaffAPI, '/groups/<int:groupid>/staff', endpoint = 'groupstaffs')
api.add_resource(StudentGroupStaffAPI, '/groups/<int:groupid>/staff/<int:staffid>', endpoint = 'groupstaff')
## >>>>> STOPPED HERE

@app.cli.command()
@click.option('--login',  help='user@mail.com')
@click.option('--password',  help='password')
@click.option('--isadmin', default="true", help='isadmin')
@click.option('--istrainer', default="true", help='istrainer')
@click.option('--name',  help='name')
@click.option('--phone',  help='phone')
@click.option('--company',  default=9, help='company')
def adduser(login, password, isadmin, istrainer, name, phone, company):
    """ Create new user"""
    admin = False
    if isadmin == 'true':
        admin = True

    if istrainer == 'true':
        trainer = True
        
    newuser = Staff(login=login, admin=admin, trainer=trainer, name=name, phone=phone, company_id=int(company))
    newuser.hash_password(password)
    newuser.is_confirmed = True
    newuser.confirmed_on = datetime.datetime.today()
    db.session.add(newuser)
    db.session.commit()
    print("New user added", newuser)


@app.cli.command()
@click.option('--name',  help='Gym 1')
def addgym(name):
    """ Create new gym"""
    newgym = Gym(name=name)
    db.session.add(newgym)
    db.session.commit()
    print("New gym added", newgym)


@app.cli.command()
@click.option('--name',  help='Org 1')
@click.option('--userid',  help='1')
def addcompany(name, userid):
    """ Create new company"""
    staff = db.session.query(Staff).filter(Staff.id==userid).first()
    newcompany = Company(name=name)
    db.session.add(newcompany)
    db.session.commit()
    staff.company_id = newcompany.id
    db.session.add(staff)
    db.session.commit()
    print("New company added", newcompany)


@app.cli.command()
@click.option('--fname',  help='File name to import from')
def importusers(fname):
    """ Import new users"""
    import csv
    import re
    with open(fname, 'r') as f:
        f.readline()
        reader = csv.reader(f, delimiter=":")
        for row in reader:
            name = row[0]
            contract_id = int(row[1])
            bdate = row[2][:10]
            if "/" in bdate:
                bdate = bdate.split("/")
            elif "." in bdate:
                bdate = bdate.split(".")
            else:
                bdate = None
            if bdate:
                bdate = "{}-{}-{}".format(bdate[2], bdate[0], bdate[1])
            print(bdate)
            note = " ".join(row[5:])
            email = row[3]
            if len(email.split(";")) > 1:
                email = email.split(";")[0]
                note = note + " emails: " + row[3]
            if len(email.split(",")) > 1:
                email = email.split(",")[0]
                note = note + " emails: " + row[3]
            email = email.strip()
            phone = row[4]
            m = "".join(re.findall("\d", phone))
            if len(m) > 1:
                phone = "+7"+m[1:11]
            newstudent = Student(name = name, email = email, phone = phone, bdate = bdate, contract_id = contract_id, note = note)
            db.session.add(newstudent)
            db.session.commit()


@app.cli.command()
def addstudents():
    # add students for the current month's lessons
    groups = db.session.query(StudentGroup).all()
    for g in groups:
        for lesson in g.lessons:
            if lesson.start >= datetime.datetime(2019, 2, 1): 
                for student in g.students:
                    if student not in lesson.students:
                        print(lesson.start, student.name, lesson.group.name, lesson.trainer.name, lesson.gym.name, student.studentgroup, lesson.group)
                        lesson.students.append(student)
                        db.session.add(lesson)
                        db.session.commit()

                        
@app.cli.command()
@click.option('--fname',  help='File name to import from')
def importgroups(fname):
    """ Import groups"""
    import pandas as pd

    outlist = []
    
    xl = pd.ExcelFile(fname)
    excl_sheets = ['Лист4', "Персональные"]
    
    for sheet in xl.sheet_names:
        if sheet not in excl_sheets:
            df = xl.parse(sheet)
            headers = df[0:6]
        
            trainer = [x for x in headers.columns if (not x.startswith("Тренер") and not x.startswith("Un"))][0].split()[0]
        
            hours, mins = map(int, [x[1] for x in headers.iloc[0].iteritems() if (isinstance(x[1], str) and ":" in x[1])][0].split()[1].strip().split(":"))
        
            dates = [x[1] for x in headers.iloc[-1].iteritems() if (isinstance(x[1], datetime.date) or isinstance(x[1], pd.Timestamp) or isinstance(x[1], datetime.datetime))]
            dates = [x.to_pydatetime() if isinstance(x, pd.Timestamp) else x for x in dates ]
            dates = [datetime.datetime.combine(x.date(), datetime.time(hours, mins)) for x in dates]

            gym = [x[1] for x in headers.iloc[1].iteritems() if (isinstance(x[1], str) and not "База" in x[1])][0].strip()
            
            group = [x[1] for x in headers.iloc[2].iteritems() if (isinstance(x[1], str) and not "Назв" in x[1])][0].strip().replace("год ", "года ")
        
            data = [x for x in df[6:-1].iloc[:, 0].dropna()]
            stdata = [x for x in df[6:-1].iloc[:, 1].dropna()]
            
            
            stdata = dict(zip(data, stdata))
            for dd in dates:
                for d in stdata:
                    student = db.session.query(Student).filter(Student.contract_id == stdata[d]).first()
                    if not student:
                        student = db.session.query(Student).filter(Student.contract_id == d.strip()).first()

                    if student:
                        #print(d, [s.name for s in student])
                        #startdate = datetime.datetime.strptime(dd, '%d.%m.%Y %H:%M:%S')
                        trainer_db = [s for s in db.session.query(Staff).all() if s.name.split()[-1] == trainer.capitalize()]
                        gymname = gym
                        if gym.lower() == "рокзона":
                            gymname = "Rock Zona Boulder House"
                        if gym.lower() == "редпоинт":
                            gymname = "Redpoint"

                        gym_db = db.session.query(Gym).filter(Gym.name == gymname).first()

                        group_db = db.session.query(StudentGroup).filter(StudentGroup.name == group.capitalize()).filter(StudentGroup.gym == gym_db).filter(StudentGroup.trainer == trainer_db[-1]).first()

                        lesson_db = db.session.query(Lesson).filter(Lesson.group == group_db).filter(Lesson.gym == gym_db).filter(Lesson.start == dd).first()
                        

                        
                        if not student.lastvalidticket:
                        # add test ticket to student
                        # append student to group
                        # append student to lesson
                            price = db.session.query(Price).filter(Price.id==14).first()
                            newticket = Ticket(student=student, note="test ticket", price=price, visitsleft=4, cancels=0, transfers=0, is_paid=True)
                            lastday = lastdate()
                            newticket.ends = lastday
                            db.session.add(newticket)
                            db.session.commit()
                        print(gym_db, trainer_db[-1].name, group_db, group, dd, student.name, student.lastvalidticket, lesson_db)         
                        if student not in group_db.students:
                            print("append student")
                            group_db.students.append(student)
                            db.session.add(group_db)
                            db.session.commit()
                        if lesson_db:
                            if student not in lesson_db.students:
                                lesson_db.students.append(student)
                                db.session.add(lesson_db)
                                db.session.commit()
                        
                        print(gym_db, trainer_db[-1].name, group_db.id, group, dd, student.name, student.lastvalidticket, lesson_db)
                            
                    outdict = {"gym": gym,
                               "trainer": trainer,
                               "group": group,
                               "date": dd,
                               "student": d
                    }
                    outlist.append(outdict)
    outdata = pd.DataFrame(outlist)
    
    print(outdata)
        # out
        # stname gym trainer group starttime
            
@app.cli.command()
@click.option('--url',  help='URL to import from')
@click.option('--fixonly')
def importlessons(url, fixonly=False):
    r = requests.get(url, timeout=20)
    c = r.content
    soup = BeautifulSoup(c, features="html.parser")
    gym = soup.find("div", class_="title-club").find("form").find_all("option", selected=True)[-1].getText()
    gym_db = db.session.query(Gym).filter(Gym.name == gym).first()
    columns = soup.find_all("div", class_="column")
    for cl in columns:
        cells = cl.find_all("div", class_="cell")
        daystart = dayend = None
        
        sitelessons = []
        
        for cell in cells:
            if cell.has_attr("data-event-start"):
                datestart = datetime.datetime.strptime(cell["data-event-start"], '%d.%m.%Y %H:%M:%S')
                dateend = datetime.datetime.strptime(cell["data-event-end"], '%d.%m.%Y %H:%M:%S')
                lnk = cell.findAll('a')[-1]
                data = {"id":lnk['data-id']}
                resp = requests.post('https://o-skal.ru/ajax/raspisanie-ajax.php', data=data)
                sp = BeautifulSoup(resp.text, features="html.parser")
                headers = sp.findAll('p', class_="h2")
                if len(headers) > 0:
                    headers = headers[0]
                hours = re.findall("\d.:\d.", headers.text)
                if len(hours) > 1:
                    hours = list(map(int, hours[-1].split(":")))

                # Fixing wrong lesson end
                dateend = dateend.replace(hour=hours[0], minute=hours[1])
                daystart = datestart.replace(hour=1, minute=0)
                dayend = dateend.replace(hour=23, minute=59)

                trainer = cell.find("p", class_="s-trainers").text.strip().split()[0].strip()
                trainer_db = [s for s in db.session.query(Staff).all() if s.name.split()[-1] == trainer]

                
                if len(trainer_db) > 0:
                    group = cell["data-event-title"]
                    group = group[group.find("(")+1:group.find(")")].capitalize()
                    prevlesson = db.session.query(Lesson).filter(Lesson.gym == gym_db).filter(Lesson.start == datestart).filter(Lesson.end == dateend).first()
                    if prevlesson:
                        grouptrainer = prevlesson.trainer
                    else:
                        grouptrainer = trainer_db[-1]
                    group_db = db.session.query(StudentGroup).filter(StudentGroup.name == group).filter(StudentGroup.gym == gym_db).filter(StudentGroup.trainer == grouptrainer).first()
                    
                    
                    if not group_db:
                        print("NO GROUP", group, trainer, datestart, dateend)
                        # create new group
                        group_db = StudentGroup(name=group, trainer=grouptrainer, gym=gym_db)
                        db.session.add(group_db)
                        db.session.commit()
                        print("NEW GROUP CREATED", group, trainer, )
                    
                    if prevlesson:
                        if prevlesson.trainer != trainer_db[-1]:
                            prevlesson.trainer = trainer_db[-1]
                        if prevlesson.group is None:
                            print("ADDING GROUP TO EXISTING LESSON", group_db)
                            prevlesson.group = group_db
                        if fixonly:
                            print("FIXING LESSON DATETIME")
                            prevlesson.end = newdateend
                        db.session.add(prevlesson)
                        db.session.commit()

                    if not prevlesson and not fixonly:
                        print("CREATING NEW LESSON", datestart, dateend, gym_db.name, trainer_db[-1].name, group)
                        newlesson = Lesson(start=datestart,
                                           end=dateend,
                                           gym=gym_db,
                                           lessontype='group',
                                           trainer=trainer_db[-1],
                                           group=group_db
                        )
                        print(newlesson.start, newlesson.end)
                        db.session.add(newlesson)
                        db.session.commit()
                        print(newlesson)
                        for st in group_db.students:
                            if st not in newlesson.students:
                                newlesson.students.append(st)
                        if newlesson not in sitelessons:
                            sitelessons.append(newlesson)
                    else:
                        if prevlesson not in sitelessons:
                            sitelessons.append(prevlesson)
        if daystart:
            todaylessons = db.session.query(Lesson).filter(Lesson.start >= daystart).filter(Lesson.end <= dayend).filter(Lesson.gym == gym_db).all()
            todelete = list(set(todaylessons) - set(sitelessons))
            # только групповые занятия!!
            # индивидуалки и мини не отображаются на сайте
            for l in todelete:
                if l.lessontype == 'group':
                    print("To delete", l.gym.name, l.trainer.name, l.start, l.end, l.group)
                    print("NOT ACTUALLY DELETING")
                    # db.session.delete(l)
                    # db.session.commit()
                else:
                    print("Not deleting", l.lessontype.name, l.gym.name, l.trainer.name, l.start, l.end, l.group)


                    
if __name__ == '__main__':
    app.run(host='0.0.0.0')
