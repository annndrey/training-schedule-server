from app import app

# gunicorn --bind localhost:6547 wsgi:app
if __name__ == "__main__":
    app.run()
    
